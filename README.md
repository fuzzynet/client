# fuzzynet

fuzzynet is a p2p antisocial content sharing network

uncensorable. decentralized. anonymous. monerotized.

don't tell us who you are, just show us what you have

disclaimer: we can make no privacy guarantees at this time

- uncensorable
- anonymous users
- monetized content

## intro

fuzzynet is a p2p antisocial (collectively anonymous) filesharing network

it aim is to help creators monetize their content through a patron model

content is shared peer-to-peer, so it's uncensorable, but content is tagged and blocked through peer gossip networks (tag sharing of content and other peers)

content is only discoverable by tag, so content undesirable to a user can easily be blacklisted

to incentivize content sharing and availability, fuzzynet will mine monero while sharing

we don't care who you are, we care about what you have to contribute:

- a computer with spare cycles and storage
- some internet bandwidth
- content you want to share and upload
- content tagging and networking with peers

## feature roadmap

current version: 0.0 (unreleased)

### 0.1 - image sharing

- [ ] add and share image content
- [ ] browse peer content via direct `fuzzynet://` links
- [ ] download and share peer content

### 0.2 - audio and video sharing

- [ ] add and share audio content
- [ ] add and share video content

### 0.3 - tagging and discovery, v1

- [ ] add and share tags
- [ ] curate (tag) content
- [ ] content discovery
- [ ] subscribe to peer content shared under a tag
- [ ] tag blacklisting
- [ ] tag synonyms, hierarchy, categories, types, associations, etc

### 0.4 - monetization, v1

- [ ] monero wallet
- [ ] monero mining
- [ ] semi-automated business-class peer-to-peer smart contracts (SABCP2PSCs, or p2p smart contracts, for short)
  - "code that determines who gets what money, when"
  - periodicity, distributions, conditions
  - attribution, anonymity, encrypted ledger

### 0.4.1 - superpeer dicovery

- [ ] superpeers
  - global listing of peers signed up to a weekly leaderboard, sorted by a blind contribution to help benefit fuzzynet development

### 0.5 - monetization, v2

- [ ] peer content monetization
- [ ] peer tags monetization

### 0.6 - stream sharing

- [ ] webrtc stream sharing
- [ ] stream recording

### mid-term

- [ ] shared content monetization
- [ ] pages for creators

### long-term

- [ ] p2p privacy - an ongoing security project

## build

you will need nodejs and yarn installed. node 12 should be fine. install yarn with `npm i -g yarn`.

a basic install script is included in `INSTALL.sh`.

once that has been run successfully, the client can be started using `yarn open`.

## contact

use a matrix client like [riot](https://about.riot.im/) to connect to our matrix channel, #fuzzynet
