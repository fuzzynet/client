import * as Types from 'types/shared'

export const ports: Types.Ports = {
  http: 18650,
  ipfs: undefined,
}

export const smallThumbSize = 256
export const largeThumbSize = 512

export const forwarder = 'http://localhost:18650/api/open'
