// shared routes between api and ui (express and fetch)

export const routes = {
  get: {
    content: (hash: string, query = '') => `/content/${hash}${query}`,
    mobile: () => '/mobile',
    discover: () => '/discover',
    tags: (hash: string) => `/tags/${hash}`,
    community: { all: () => '/community/all' },
    state: () => '/state',
    share: (content: string) => `/share/${content}`,
    open: (content: string) => `/open/${content}`,
  },
  post: {
    account: { new: () => '/account/new' },
    content: () => '/content',
    tags: () => '/tags',
    community: {
      switch: (community: string) => `/community/switch/${community}`,
    },
    share: (content: string) => `/share/${content}`,
  },
}
