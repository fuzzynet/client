export interface KV<T> {
  [k: string]: T
}

export type ReduxData = any

export interface ReduxAction<T extends ReduxData> {
  type: string
  data: T
}

export type ActionCreator = (data?: ReduxData) => string

// fn always returns T only if defaultValue is undefined, otherwise, return a maybe T
export type Maybe<T> = T | undefined

export type FileContent = Object | Blob | string

export interface IPFSFile {
  path: string
  hash: string
  size: number
  content?: FileContent
}

export interface IdentityJson {
  id: string
  publicKey: string
  signatures: { id: string; publicKey: string }
  type: string
}

export interface LamportClockJson {
  id: 'string'
  time: number
}

export interface LogEntry<T> {
  hash: string
  id: string
  payload: { op?: string; key?: string; value: T }
  next: string[] // Hashes of parents
  v: number // Format, can be 0 or 1
  clock: LamportClockJson
  key: string
  identity: IdentityJson
  sig: string
}
