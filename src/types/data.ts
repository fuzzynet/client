import { KeyObject } from 'crypto'

import * as IPFS from 'ipfs'
import OrbitDB from 'orbit-db'
import DocumentStore from 'orbit-db-docstore'
import FeedStore from 'orbit-db-feedstore'
import KVStore from 'orbit-db-kvstore'
import LogStore from 'orbit-db-eventstore'
import * as WebSocket from 'ws'

import { ReduxStore } from 'store'
import * as Shared from 'types/shared'
import * as Schemas from 'types/schemas'
import { Snap } from 'data/snap'

export interface IPFSD {
  api: IPFS
  start: () => Promise<void>
  stop: () => Promise<void>
  init: () => Promise<void>
  error: boolean
  ready: Promise<void>
}

export type Doc<T> = Schemas.RecordID & T

export type IterableOrbitStore<T> = FeedStore<T> | LogStore<T>
export type OrbitDocumentStore<T> = DocumentStore<Doc<T>>
export type OrbitFeedStore<T> = FeedStore<T>
export type OrbitKVStore<T> = KVStore<T>
export type OrbitLogStore<T> = LogStore<T>

export type OrbitStore<T> =
  | OrbitDocumentStore<T>
  | OrbitFeedStore<T>
  | OrbitKVStore<T>
  | OrbitLogStore<T>

export interface Model {
  account: {
    local: OrbitDocumentStore<Shared.Account>
    peers: OrbitDocumentStore<Shared.Account>
  }
  content: OrbitDocumentStore<Schemas.Content> // TODO: apogeedb fix
  tags: OrbitDocumentStore<Schemas.Tag>
  subscriptions: OrbitDocumentStore<Schemas.Subscription>
  feeds: {
    personal: OrbitFeedStore<Schemas.ID> // my content
    external: OrbitFeedStore<Schemas.ID> // from subscriptions
  }
  log: OrbitLogStore<Schemas.ActionEvent>
}

export interface Crypto {
  jwk: KeyObject
  account: string
}

export type WSBroadcast = (account: string, action: string) => void

export interface Resources {
  cpus: number
  ports: Shared.Ports
  folder: string
  appPath: string
  store: Snap
  ipfs: IPFS
  orbitdb: OrbitDB
  model: Model
  crypto: Crypto
  wsUser: Map<string, WebSocket>
  wsAccount: Map<string, string[]>
  wsBroadcast: WSBroadcast
  redux: ReduxStore
  dispatch: (action: Schemas.ActionEvent, user: string) => Promise<void>
}

export interface Token {
  user: string
  account: string
}
