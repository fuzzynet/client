# types

documentation specifically for fuzzynet client types.

## type entry points between api and ui

there are two primary typings entry points that should be imported:

- types for the frontend, ui.ts, so the Types import there should only import from `../types/ui`
- types for the backend, api.ts, so the Types import there should only import from `../types/api`

this is done because some types such as IPFS and electron types should not import code to be bundled by webpack for the ui. separation of code that is meant only for node.js and code meant only for the browser sometimes needs to occur, even in TypeScript typing imports.

take care not to import from other type files directly. `./api.ts` and `./ui.ts` should be considered 'index' files for their respective concerns.

other types can be shared between the two, such as lib and shared.

lib is for 'utility' typings, where shared is more for request and response typings.

finally, there's also `shared.ts`, which is safe to import in ui and api files, and included in both type entry points.

## schemas

runtime type checks are kept in `schemas.ts`

it's important to check types that are persisted or communicated via schema-style runtime type checks to ensure data shape and future migration.
