import * as t from 'io-ts'

// Record ID
export const ID = t.string
export type ID = t.TypeOf<typeof ID>

// User (wip)
export const User = t.type({
  userId: t.number,
  name: t.string,
})

// Content
export const Content = t.type({
  _id: ID, // Original content hash
  type: t.string, // Content type (image, video, audio, text, file, stream)
  thumb1: t.string, // Small thumbnail hash, video image thumbnail, audio lowres album art
  thumb2: t.string, // Large thumbnail hash, video clips thumbnail, audio highres album art
  width: t.number,
  height: t.number,
  length: t.number, // seconds for audio and video, pixel count for images
})
export type Content = t.TypeOf<typeof Content>

// FeedType
export const FeedType = t.union([t.literal('personal'), t.literal('external')])
export type FeedType = t.TypeOf<typeof FeedType>

// ActionEvent
export const ActionEvent = t.type({
  type: t.string,
  payload: t.any,
})
export type ActionEvent = t.TypeOf<typeof ActionEvent>

// Record
export const RecordID = t.type({
  _id: ID,
})
export type RecordID = t.TypeOf<typeof RecordID>

export const Record = <T extends t.Mixed>(codec: T) =>
  t.intersection([RecordID, codec])
export type Record = <T extends t.Mixed>(
  codec: T,
) => T & t.TypeOf<typeof RecordID>

// Tag
export const Tag = t.type({
  content: ID,
  tag: ID,
})
export type Tag = t.TypeOf<typeof Tag>

// Subscription
export const Subscription = t.type({
  tag: ID,
  bar: ID, // TODO: wtf
})
export type Subscription = t.TypeOf<typeof Subscription>
