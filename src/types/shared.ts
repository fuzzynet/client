export interface Account {
  keys: any // TODO: account keys
  emoji: string
}

export interface GetMobileResponse {
  qr: {
    lan: string
    // ngrok: string
    // natpmp: string
  }
  url: {
    lan: string
    // ngrok: string
    // natpmp: string
  }
  err: {
    lan: boolean
    // ngrok: boolean
    // natpmp: boolean
  }
}

export type PostContentRequest = string[]

export type PostTagsRequest = string[]
export type PostTagsResponse = string[]

export type PostTagsSearchRequest = string
export type PostTagsSearchResponse = string

export type GetFeedResponse = string[]

export type Routes =
  | 'home'
  | 'mobile'
  | 'discover'
  | 'curate'
  | 'share'
  | 'mine'
  | 'account'
  | 'help'
  | 'switch'

export * from 'types/lib'
export * from 'types/schemas'

export interface Ports {
  http: number
  ipfs?: number
}
