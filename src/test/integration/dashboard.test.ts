/// <reference types="cypress" />

/*
EACH CONTEXT HANDLES EACH
FEATURE ON THE DASHBOARD:
FEEDS, USER PROFILE, B/W-LIST, WALLET
*/

describe.skip('landing page/DASHBOARD', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  //THIS PART TESTS THE FEEDS PAGE AND ITS FEATURES
  context('testing FEEDS link on DASHBOARD', () => {
    it('loads FEEDS page', () => {
      cy.contains('.feed-nav a', 'feeds').click()

      cy.get('.main-feed')
        /* HERE I NEED TO WRITE A TEST THAT VERIFIES THE
          CORRECT NUMBER OF ITEMS IS BEING SHOWN,
          ACCORDING TO USER'S SETINGS*/

        //https://stackoverflow.com/questions/46850694/in-cypress-how-to-count-a-selection-of-items-and-get-the-length
        .should('have.length', 'USER SETTINGS')

      // HERE WE INTERACT WITH THE CONTENT FILTER DROP-DOWN
      cy.get('.filter')
        .should('have.value', 'Content Type')
        .click()

      //we click a data-type to filter only "images" for the main FEEDS display
      cy.get('li')
        .should('have.value', 'images')
        .click()

      // HERE WE VERIFY THAT ONLY IMAGE FILES ARE BEING DISPLAYED
      // HERE'S MY REFERENCE:
      // https://stackoverflow.com/questions/46850694/in-cypress-how-to-count-a-selection-of-items-and-get-the-length
      // I DON'T KNOW WHICH OF THESE TESTS WILL PASS/FAIL

      cy.get('.main-feed')
        .find('.mainFeedFile')
        .should('not.contain.value', 'USER SETTING')

      cy.get('.main-feed')
        .find('.mainFeedFile')
        .should('not.')
    })
  })

  //THIS PART TESTS THE PROFILE PAGE AND ITS FEATURES
  context('testing PROFILE link on DASHBOARD', () => {
    it('loads FEEDS page', () => {
      cy.contains('.feed-nav a', 'profile').click()
    })
  })

  //THIS PART TESTS THE TAG WHITE/BLACK-LIST PAGE AND ITS FEATURES
  context('testing TAGLIST link on DASHBOARD', () => {
    it('loads FEEDS page', () => {
      cy.contains('.feed-nav a', 'taglist').click()
    })
  })

  //THIS PART TESTS THE WALLET PAGE AND ITS FEATURES
  context('testing WALLET link on DASHBOARD', () => {
    it('loads FEEDS page', () => {
      cy.contains('.feed-nav a', 'wallet').click()
    })
  })

  // TODO: fix these tests, pls
  /*
  it('loads the switch page', function() {
    cy.visit('/switch')
    // cy.get('h2:first-child').should('have.text', 'communities') // TODO: refactor for personal and external instead of communities
    // cy.get('li:first-child button').click({ force: true })
    cy.get('li')
      .contains('subscribe')
      .click()
    // cy.contains('subscribe').click()
    // cy.contains('unsubscribe').click()
  })
  })
  */
})
