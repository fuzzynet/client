/// <reference types="cypress" />

const cwd = Cypress.env('CWD')

const fuzzynetFaviconThumb2 =
  '.feed .feed-item img[src="/api/content/QmWbcxwKpPFJDbNPryRnzN51HwofLtCtxWXN1zK9b4csvd"][height="256"][width="256"]'
const fuzzynetFaviconOriginal =
  '#content-view img[src="/api/content/QmP6FMQcCtVf5b2L2BXLSe2eRwWN4GE7uZZ853cvHQQQuo"][height="128"][width="128"]'

const fetchFiles = (...files: string[]) => {
  fetch('http://127.0.0.1:18650/api/content', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(files.map((file) => `${cwd}/${file}`)),
  })
}

describe('share page', () => {
  it('can upload a single image and display it in a feed', () => {
    cy.visit('/')
      .contains('#nav a', 'share')
      .click()

    fetchFiles('static/favicon.png')

    cy.get('.feed-item img').should('have.length.gte', 1)
    cy.get('.feed-item img').should('have.attr', 'width')
    cy.get('.feed-item img').should('have.attr', 'height')
  })

  it("should only show an image once, even it's been uploaded multiple times.", () => {
    fetchFiles('static/favicon.png')
    cy.wait(1000)
    fetchFiles('static/favicon.png')
    cy.wait(1000)
    fetchFiles('static/favicon.png')
    cy.wait(1000)
    cy.get(fuzzynetFaviconThumb2).should('have.length', 1)
  })

  it('should open content view for image when left-clicked', () => {
    cy.get(fuzzynetFaviconThumb2).click()
    cy.get(fuzzynetFaviconOriginal).should('exist')
  })

  it('should close content-view when content view is clicked', () => {
    cy.get('.feed .feed-item img').should('not.exist')
    cy.get('#content-view img').click()
    cy.get('.feed .feed-item img').should('have.length.gte', 1)
  })

  it('should close content-view when escape key is pressed', () => {
    cy.get(fuzzynetFaviconThumb2).click()
    cy.get('body').type('{esc}')
  })
})
