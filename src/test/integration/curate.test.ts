/// <reference types="cypress" />

describe.skip('landing page', () => {
  it('loads the main page', () => {
    cy.visit('/')
      .contains('#nav a', 'curate')
      .click()
    cy.get('.input')
      //we type in a tag then click the "add button"
      .should('have.value)', 'enter comma separated tags')
      .type('cats')

    cy.get('.tag-button').click()

    //we check that our submitted tag is in the tag-list field
    cy.get('.tag-list').should('have.text', 'cats')

    //we check that user's tag watch-list is present
    cy.get('.all-tags').should('have.text', 'document tags')

    //we click the dropdown and select a content filter
    cy.get('.button')
      .should('have.value', 'Content Type')
      .click()

    //we click a type to load into display "discover"
    cy.get('li')
      .should('have.value', 'documents')
      .click()

    cy.get('.tag-list')
      .contains('li', 'Taggy')
      .click()

    //I need to figure out this last part
    cy.get('.discover').should('have.length', '?')

    cy.get('.discover').should('have.attr', 'width')

    cy.get('.discover').should('have.attr', 'height')
  })
})
