/// <reference types="cypress" />

describe.skip('connect page', () => {
  beforeEach(() => {
    cy.visit('/connect')
  })

  // TODO: change these to not use your own LAN IP pls thx

  /* this is one type of test I found
    https://github.com/cypress-io/cypress-example-recipes/blob/master/examples/testing-dom__tab-handling-links/cypress/integration/tab_handling_anchor_links_spec.js */
  context('testing target="_blank" internal link', () => {
    it('verifies the hfref but no click', () => {
      cy.get('a')
        .should('have.attr', 'href')
        .and('include', 'http://192.168.1.2:18650/')
      //not sure if second half of this test is necessary
      cy.get('a')
        .should('have.prop', 'href')
        .and('equal', 'http://192.168.1.2:18650/')
    })
  })

  //THIS IS ANOTHER WAY TO TEST FOR TARGET_BLANK
  context('testing target="_blank" proxy link', () => {
    it('verifies the hfref but no click', () => {
      cy.get('a')
        .should('have.attr', 'href')
        .and('include', 'https://9ff1472d.ngrok.io')
      //not sure if second half of this test is necessary
      cy.get('a')
        .should('have.prop', 'href')
        .and('equal', 'https://9ff1472d.ngrok.io')
    })
  })
})

/*  THIS IS THE OLD TEST
    it('loads the connect page', () => {
      cy.get('')
        .contains('mobile link')
        //we try to click the "internet network link"
        .get('a')
        .should('have.value', 'http://192.168.1.21:18650/')
        .click() */
