export const getCookie = (name: string): string => {
  const nameLenPlus = name.length + 1
  return document.cookie
    .split(';')
    .map((c) => c.trim())
    .filter((cookie) => {
      return cookie.substring(0, nameLenPlus) === `${name}=`
    })
    .map((cookie) => {
      return decodeURIComponent(cookie.substring(nameLenPlus))
    })[0]
}

export type EventListener = (evt: any) => void

export const createListener = (
  target: any,
  event: string,
  listener: EventListener,
) => () => {
  target.addEventListener(event, listener)
  return () => target.removeEventListener(event, listener)
}
