import * as React from 'react'
import { useEffect } from 'react'

import { useSelector } from 'react-redux'
import { RootState } from 'store'

import * as api from 'ui/api/http'
import { Dropdown } from 'pages/Dropdown'

//const dispatch = useDispatch
type Props = {
  displayMainAction: any
}

const handleTagsSubmit = async (
  setTagItemsList: React.Dispatch<React.SetStateAction<string[]>>,
  tags: string,
) => {
  const tagsList = tags.split(',')
  const tagsResponse = await api.postTagsDiscover(tagsList)
  setTagItemsList(tagsResponse)
  //
}

const TagList = ({ displayMainAction }: Props) => {
  const [tagList, setTagsList] = React.useState<string>('myTag')
  const [tagItemsList, setTagItemsList] = React.useState<string[]>([])

  return (
    <>
      <div>
        <div className="tag-section">
          <input
            className="tag-text-area"
            type="text"
            readOnly={false}
            defaultValue="enter comma seperated tags"
            onChange={(e) => setTagsList(e.target.value)}
            onFocus={(e) => (e.target.value = '')}
          />
          <button
            className="tag-button"
            onClick={() => handleTagsSubmit(setTagItemsList, tagList)}
          >
            add tags
          </button>
        </div>
      </div>
      <ul className="tag-list">
        {tagItemsList.map((tag, i) => (
          <li key={i}>{tag} (1)</li>
        ))}
      </ul>
      <div>
        <Dropdown action={displayMainAction} />
      </div>
    </>
  )
}

export const Curate = () => {
  const discoverTags = useSelector(
    (state: RootState) => state.discoverTags.showTags,
  )

  const [tagAllItemsList, setAllTagItemsList] = React.useState<string>()
  const [displayMain, setDisplayMain] = React.useState<boolean>(true)

  useEffect(() => {
    const getAllTages = JSON.stringify(api.getAllTagsData())
    setAllTagItemsList(getAllTages)

    console.log('EFFECT DISCOVER CHANGE')
    console.log('discoverTags ', discoverTags)
  })

  const handleDisplayMain = (): any => {
    setDisplayMain(false)
    console.log('handleDisplayMain >> ', discoverTags)
    console.log('discoverTags ', discoverTags)
  }

  return (
    <div className="discover">
      <div className="tagList">
        <TagList displayMainAction={handleDisplayMain} />
      </div>
      {displayMain ? (
        <div className="main-content">
          <div>
            <div className="header">all my damn tags</div>
          </div>
          <div className="all-tags">
            <h4>document tags</h4>
            <div>Taggy, Scareplane, Trucks, Turtles, Dogs, Cats, Bannanas</div>
            <h4>image tags</h4>
            <div>
              Country, Scareplane, Trucks, Turtles, Dogs, Cats, Vacations
            </div>
            <h4>audio tags</h4>
            <div>country, rock, symphony, podcasts</div>
            <h4>video tags</h4>
            <div>
              Horror, Comedy, Movies, Tutorials, Music, Documentory, serials
            </div>
            <h4>software tags</h4>
            <div>Programs, EIC, ISO</div>
          </div>
          <div>{tagAllItemsList}</div>
        </div>
      ) : (
        <p>select content type to load</p>
      )}

      {/* <div className="main-pics">
          <div className="thumb-box">image goes here</div>
          <div className="thumb-box">image goes here</div>
          <div className="thumb-box">image goes here</div>

          <div className="thumb-box">image goes here</div>
          <div className="thumb-box">image goes here</div>
          <div className="thumb-box">image goes here</div>

          <div className="thumb-box">image goes here</div>
          <div className="thumb-box">image goes here</div>
        </div> */}
    </div>
  )
}
