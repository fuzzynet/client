import * as React from 'react'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { Dispatch } from '@reduxjs/toolkit'

// import { RootState } from 'store'
import * as api from 'ui/api/http'
import { actions } from 'store'

type Props = {
  action: any
}

const handleFilterClick = async (
  setTagList: any,
  filterValue: string,
  displayMenu: boolean,
  action: any,
  dispatch: Dispatch,
) => {
  const res = api.getFilteredTagData(filterValue)
  //console.log('res >>> ', res) // to show the entire response

  //setShowTagList(true) this displays the filtered tag list
  setTagList(res)

  // this should work according to that docs
  console.log('displayMenu >>> ', displayMenu)
  //setShowTags(false)
  console.log('<<< LOAD FILTERED IMAGES >>> >>> ')
  dispatch(actions.discoverTags.setShowTags(true))
  // dispatch resets main to display true to load filtered images.
  // need to call dispatch handler with action again
  dispatchHandler(action, dispatch)
}

// dispatch handler allows discover to reset main with value from redux
const dispatchHandler = (action: any, dispatch: Dispatch) => {
  console.log('<<< CALLBACK TO DISCOVER TO SHOW MAIN >>> >>> ')
  dispatch(actions.discoverTags.setShowTags(false))
  return action
}

export const Dropdown = ({ action }: Props) => {
  const [displayMenu, setDisplayMenu] = useState<boolean>(false)
  const [tagList, setTagList] = useState<string[]>([])
  const dispatch = useDispatch()

  // const dropdownTODO = useSelector((state: RootState) => state.dropdownTODO)

  return (
    <>
      <div
        className="dropdown"
        onClick={() => setDisplayMenu(displayMenu === true ? false : true)}
      >
        <div className="button" onClick={dispatchHandler(action, dispatch)}>
          Content Type
        </div>

        {displayMenu ? (
          <ul>
            <li
              onClick={() => {
                handleFilterClick(
                  setTagList,
                  'document',
                  displayMenu,
                  action,
                  dispatch,
                )
              }}
            >
              documents
            </li>
            <li
              onClick={() => {
                handleFilterClick(
                  setTagList,
                  'image',
                  displayMenu,
                  action,
                  dispatch,
                )
              }}
            >
              images
            </li>
            <li
              onClick={() => {
                handleFilterClick(
                  setTagList,
                  'audio',
                  displayMenu,
                  action,
                  dispatch,
                )
              }}
            >
              audio
            </li>
            <li
              onClick={() => {
                handleFilterClick(
                  setTagList,
                  'video',
                  displayMenu,
                  action,
                  dispatch,
                )
              }}
            >
              video
            </li>
            <li
              onClick={() => {
                handleFilterClick(
                  setTagList,
                  'software',
                  displayMenu,
                  action,
                  dispatch,
                )
              }}
            >
              software
            </li>
          </ul>
        ) : null}
      </div>
      <div className="displayTagList">
        <ul className="tag-list">
          {tagList.map((tag, i) => (
            <li key={i}>{tag} (1)</li>
          ))}
        </ul>
      </div>
    </>
  )
}
