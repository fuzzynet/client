import * as React from 'react'
import { useCurrentRoute } from 'react-navi'

import * as Types from 'types/ui'

export const Mobile = () => {
  const route = useCurrentRoute()
  const { qr, url, err } = route.data as Types.GetMobileResponse

  return (
    <>
      {!err.lan && (
        <>
          <h2>mobile link</h2>
          <p>internal network link (persistent, local only):</p>
          <p>
            <a href={url.lan} target="_blank">
              {url.lan}
            </a>
          </p>
          <p>
            <img src={qr.lan} />
          </p>
        </>
      )}
      {/*
      {!err.natpmp && (
        <>
          <p>external network link (temporary, 2 hours):</p>
          <p>
            <a href={url.natpmp} target="_blank">
              {url.natpmp}
            </a>
          </p>
          <p>
            <img src={qr.natpmp} />
          </p>
        </>
      )}

      {!err.ngrok && (
        <>
          <p>http proxy link (temporary, 8 hours):</p>
          <p>
            <a href={url.ngrok} target="_blank">
              {url.ngrok}
            </a>
          </p>
          <p>
            <img src={qr.ngrok} />
          </p>
        </>
      )} */}
    </>
  )
}
