import * as React from 'react'

import { Layout } from 'components/Layout'

interface Props {}

interface State {
  hasError: boolean
}

export const Error = () => (
  <Layout ws={undefined}>
    <h1>something went wrong</h1>
    <p>try restarting the app</p>
  </Layout>
)

export class ErrorBoundary extends React.Component<Props, State> {
  state = { hasError: false }

  static getDerivedStateFromError(err: string) {
    console.error(err)
    return { hasError: true }
  }

  componentDidCatch(err: Error, info: React.ErrorInfo) {
    console.error(err, info)
  }

  render() {
    if (this.state.hasError) {
      return <Error />
    }

    return this.props.children
  }
}
