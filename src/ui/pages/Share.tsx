import * as React from 'react'
import { useSelector } from 'react-redux'
import { RootState } from 'store'

import { ResizableFeed } from 'components/Feed'
import { ActionButton } from 'components/ActionButton'

export const Share = () => {
  const shareStatus = useSelector((state: RootState) => state.shareStatus)

  return (
    <div className="share">
      <div className="action-nav">
        <h2>add files</h2>
        <ActionButton action="select files" />
        <ActionButton action="select folder" />

        {shareStatus.active ? (
          <p>
            processing {shareStatus.count} of {shareStatus.total} files
          </p>
        ) : (
          <p>no files selected</p>
        )}
      </div>
      <ResizableFeed feedType="personal" />
    </div>
  )
}
