import * as React from 'react'
import { Link } from 'react-navi'

export const Account = () => {
  return (
    <div>
      <p>
        you can have multiple accounts associated with a page, but only one
        account per machine
      </p>
      <p>
        individual accounts are not associated with shared content, only pages
      </p>
      <p>pages have wallets that can receive monero from supporters</p>
      <p>
        an <i>account card</i> is an image containing account keys that you
        download and save somewhere safe
      </p>
      <p>
        it gives anyone with that card full access to contribute something on
        your behalf
      </p>
      <p>
        we'll do our best not to let you upload and share your account cards
      </p>
      <p>be sure to save and distribute them "out-of-band"</p>
      <p>you'll get one account card per machine</p>
      <p>
        you'll need another account card associated with a page to add another
        account card to contribute to that page
      </p>
      <p>
        there was no account already on this machine, so fuzzynet generated one
        for you
      </p>
      <p>
        <Link href="/api/account/export">download your account card</Link>
      </p>
      <p>
        <Link href="/account">next</Link>
      </p>
    </div>
  )
}
