import * as React from 'react'

const handleSubscribeSubmit = (
  e: any,
  setCommunitiesList: any,
  communitiesList: any,
  subscribed: string,
) => {
  const targeValue = e.target.value
  console.log('INCOMING TARGET :: ', targeValue)
  console.log('INCOMING LIST ', communitiesList)

  let update_array = communitiesList.map((el: any) =>
    el.comm === targeValue
      ? Object.assign({
          comm: targeValue,
          subscribed: subscribed === 'subscribe' ? 'unsubscribe' : 'subscribe',
        })
      : el,
  )

  console.log('UPDATED Community List', update_array)
  localStorage.setItem('subscribedCommunity', update_array)

  const getCommunity = localStorage.getItem('subscribedCommunity')
  setCommunitiesList(update_array)
  //console.log('newList ', newList)

  console.log('get Subscribed Community List from storage ', getCommunity)
}

export const Switch = () => {
  const [communitiesList, setCommunitiesList] = React.useState([
    { comm: 'coder', subscribed: 'subscribe' },
    { comm: 'furry', subscribed: 'unsubscribe' },
    { comm: 'queer', subscribed: 'subscribe' },
    { comm: 'music', subscribed: 'unsubscribe' },
  ])

  return (
    <div className="switch">
      <h2>select community</h2>
      <ul className="community-list">
        {communitiesList.map(({ comm, subscribed }) => (
          <li key={comm}>
            <div>
              <span className="file-text"> {comm}</span>
              <button
                className="file-button"
                value={comm}
                onClick={(e) =>
                  handleSubscribeSubmit(
                    e,
                    setCommunitiesList,
                    communitiesList,
                    subscribed,
                  )
                }
              >
                {subscribed}
              </button>
            </div>
          </li>
        ))}
      </ul>
    </div>
  )
}
