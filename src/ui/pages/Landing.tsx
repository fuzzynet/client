import * as React from 'react'

import { ResizableFeed } from 'components/Feed'

export const Landing = () => {
  return (
    <div>
      <p>welcome to fuzzynet</p>
      <div>
        if you have any content you've received from others, it will be listed
        below.
      </div>
      <ResizableFeed feedType="external" />
    </div>
  )
}
