import * as ReactDOM from 'react-dom'

import * as API from 'ui/api/http'
import { app } from 'ui/App'

const init = async () => {
  const data = await API.getState()
  ReactDOM.render(app(data), document.querySelector('#root'))
}

init()
