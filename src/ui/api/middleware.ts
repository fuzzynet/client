import { Middleware } from '@reduxjs/toolkit'

import * as Types from 'types/ui'

import * as customActions from 'store/customActions'

export const renameWebsocketActions: Middleware = (_store) => (next) => (
  action,
) => {
  if (action.type.startsWith('REDUX_WEBSOCKET')) {
    action.meta.timestamp = action.meta.timestamp.toString()
  }

  if (action.type === 'REDUX_WEBSOCKET::MESSAGE') {
    const data = JSON.parse(action.payload.message) as Types.ActionEvent
    const { type, payload } = data

    if (type.startsWith(customActions.bypassRedux())) {
      // bypass redux
      customActions.handlers[type.replace(customActions.bypassRedux(), '')](
        data,
      )
    } else {
      // normal
      return next({ type, payload })
    }
  } else {
    return next(action)
  }
}
