import * as Types from 'types/ui'
import { RootState } from 'store'

// TODO: remove, pls
const actionData = [
  {
    actions: [
      {
        docType: 'select files',
        data: ['this is a jpeg file'],
      },
      {
        docType: 'select folder',
        data: ['from folder files'],
      },
    ],
  },
]

const allTagsData = [
  {
    tagsList: [
      {
        docType: 'document',
        data: [
          'Taggy',
          'Scareplane',
          'Trucks',
          'Turtles',
          'Dogs',
          'Cats',
          'Bannanas',
        ],
      },
      {
        docType: 'image',
        data: [
          'Country',
          'Scareplane',
          'Trucks',
          'Turtles',
          'Dogs',
          'Cats',
          'Vacations',
        ],
      },
      {
        docType: 'audio',
        data: ['country', 'rock', 'symphony', 'podcasts'],
      },
      {
        docType: 'video',
        data: [
          'Horror',
          'Comedy',
          'Movies',
          'Tutorials',
          'Music',
          'Documentory',
          'serials',
        ],
      },
      {
        docType: 'software',
        data: ['Programs', 'EIC', 'ISO'],
      },
    ],
  },
]

export const prefix = (route: string) => `/api${route}`

export const getMobile = async (): Promise<Types.GetMobileResponse> => {
  const res = await fetch(prefix(Types.routes.get.mobile()))
  return await res.json()
}

export const getState = async (): Promise<RootState> => {
  const res = await fetch(prefix(Types.routes.get.state()))
  return await res.json()
}

export const postContent = async (
  filesListed: Types.PostContentRequest,
): Promise<void> => {
  await fetch(prefix(Types.routes.post.content()), {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(filesListed),
  })
}

// this develops the call by the filter drop down
export const getActionData = (filterValue: string) => {
  console.log('API filter call ', filterValue)
  // use filter here to return filtred data set
  var getFilteredData: string[] = actionData[0].actions[0].data
  const filteredDocType = actionData[0].actions
  for (var i = 0; i < filteredDocType.length; i++) {
    if (filterValue === actionData[0].actions[i].docType) {
      getFilteredData = actionData[0].actions[i].data
    }
  }

  console.log('API ACTION DATA ', getFilteredData)

  return getFilteredData
}

// this develops the call by the filter drop down
export const getFilteredTagData = (filterValue: string) => {
  console.log('API filter call ', filterValue)
  // use filter here to return filtred data set
  var getFilteredData: string[] = allTagsData[0].tagsList[0].data
  const filteredDocType = allTagsData[0].tagsList
  for (var i = 0; i < filteredDocType.length; i++) {
    if (filterValue === allTagsData[0].tagsList[i].docType) {
      getFilteredData = allTagsData[0].tagsList[i].data
    }
  }

  // console.log('API getFilteredTagData DATA ', getFilteredData)

  return getFilteredData
}

export const getAllTagsData = async () => {
  const getBodyData = JSON.stringify(allTagsData[0].tagsList)
  //const docType = JSON.parse(getBodyData)
  console.log('API getAllTagsData ', getBodyData)

  return getBodyData
}

export const postTagsDiscoverSearch = async (
  tagsListed: Types.PostTagsSearchRequest,
): Promise<Types.PostTagsSearchResponse> => {
  const res = await fetch(prefix(Types.routes.post.tags()), {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(tagsListed),
  })
  return await res.json()
}

export const postTagsDiscover = async (
  tagsListed: Types.PostTagsRequest,
): Promise<Types.PostTagsResponse> => {
  const res = await fetch(prefix(Types.routes.post.tags()), {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(tagsListed),
  })
  return await res.json()
}

export const getShareLink = async (content: string) => {
  const res = await fetch(prefix(Types.routes.get.share(content)))
  return await res.text()
}
