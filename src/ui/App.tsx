import * as React from 'react'
import { Router, View } from 'react-navi'
import { Provider } from 'react-redux'
import reduxWebsocket from '@giantmachines/redux-websocket'

import { Error, ErrorBoundary } from 'pages/Error'
import { Layout } from 'components/Layout'
import { routes } from 'ui/routes'
import { renameWebsocketActions } from 'ui/api/middleware'
import store, { RootState } from 'store'

const reduxWebsocketMiddleware = reduxWebsocket()
const prependedMiddleware = [renameWebsocketActions]
const appendedMiddleware = [reduxWebsocketMiddleware]

export const app = (preloadedState: RootState) => (
  <Provider
    store={store({ preloadedState, prependedMiddleware, appendedMiddleware })}
  >
    <Router routes={routes}>
      <ErrorBoundary>
        <Layout>
          <React.Suspense fallback={<Error />}>
            <View />
          </React.Suspense>
        </Layout>
      </ErrorBoundary>
    </Router>
  </Provider>
)
