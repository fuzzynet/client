import * as React from 'react'
import { mount, route } from 'navi'

import { Account } from 'pages/Account'
import { Curate } from 'pages/Curate'
import { Discover } from 'pages/Discover'
import { Help } from 'pages/Help'
import { Landing } from 'pages/Landing'
import { Mine } from 'pages/Mine'
import { Mobile } from 'pages/Mobile'
import { Share } from 'pages/Share'
import { Switch } from 'pages/Switch'
import * as API from 'ui/api/http'

export const routes = mount({
  '/': route({
    title: 'home',
    view: <Landing />,
  }),
  '/mobile': route({
    title: 'mobile',
    view: <Mobile />,
    getData: () => API.getMobile(),
  }),
  '/discover': route({
    title: 'discover',
    view: <Discover />,
  }),
  '/curate': route({
    title: 'curate',
    view: <Curate />,
  }),
  '/share': route({
    title: 'share',
    view: <Share />,
    // getData: () => API.getState(), // TODO: fix reload state when routing to /share
  }),
  '/mine': route({
    title: 'mine',
    view: <Mine />,
  }),
  '/account': route({
    title: 'account',
    view: <Account />,
  }),
  '/help': route({
    title: 'help',
    view: <Help />,
  }),
  '/switch': route({
    title: 'switch',
    view: <Switch />,
  }),
})
