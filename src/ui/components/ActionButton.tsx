import * as React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { Dispatch } from '@reduxjs/toolkit'
// import { RootState } from 'store'
import * as api from '../../ui/api/http'
import { actions } from '../../store'

import { useSelector } from 'react-redux'

import { RootState } from 'store'

type Props = {
  action: any
}

interface FilePath extends File {
  path: string
}

interface FilePathList {
  readonly length: number
  item(index: number): FilePath | null
  [index: number]: FilePath
}

interface FilePathListTarget extends EventTarget {
  files: FilePathList
}

interface FilePathListTargetEvent extends Event {
  target: FilePathListTarget
}

const mapFilesList = (list: FilePathList): string[] => {
  const results = []
  for (let i = 0; i < list.length; i++) {
    results.push(list[i].path)
  }
  return results
}

const handleChange = (evt: FilePathListTargetEvent) => {
  api.postContent(mapFilesList(evt.target.files))
}

const uploadHandler = (selectFolders: boolean) => {
  const input = document.createElement('input')
  input.setAttribute('type', 'file')
  input.setAttribute('multiple', 'true')
  if (selectFolders) {
    input.setAttribute('webkitdirectory', 'true')
  }
  input.addEventListener('change', handleChange as any)
  input.click()
  console.info(input, JSON.stringify(input)) // do not remove, this is a load-bearing log
}

const handleActionButtonClick = async (
  setActionData: any,
  filterValue: string,
  activeState: boolean,
  action: any,
  dispatch: Dispatch,
) => {
  filterValue === 'select folder' ? uploadHandler(true) : uploadHandler(false)
  //uploadHandler(false)
  const res = api.getActionData(filterValue)
  console.log('res >>> ', res) // to show the entire response
  console.log('action >>> ', action) // is a label
  console.log('filterValue >>> ', filterValue) // is a label

  //setShowTagList(true) this displays the filtered tag list
  setActionData(res)

  // show the button state
  console.log('activeState >>> ', activeState)
  //setShowTags(false)
  console.log('<<< LOAD FILTERED IMAGES >>> >>> ')
  dispatch(actions.discoverTags.setShowTags(true))
}

export const ActionButton = ({ action }: Props) => {
  const shareStatus = useSelector((state: RootState) => state.shareStatus)

  const [activeState, setActiveState] = useState<boolean>(false)
  const [actionData, setActionData] = useState<string[]>([])
  const [buttonLabel, setButtonLabel] = useState<string>('select files')
  const [percentage, setPercentage] = useState<number>(0)

  const dispatch = useDispatch()

  useEffect(() => {
    setButtonLabel(action)

    let actionPercent = ((shareStatus.count - 1) / shareStatus.total) * 100
    console.log('after PERCENTAGE', actionPercent)
    console.log('actionData', actionData)

    if (shareStatus.count > 1 && shareStatus.count < shareStatus.total) {
      setPercentage(actionPercent)
    }
    if (actionPercent === 100) {
      setPercentage(100)
    }
  })

  return (
    <>
      <div
        className="action-button"
        onClick={() => setActiveState(activeState === true ? false : true)}
      >
        {activeState ? (
          <div
            className="in-action"
            style={{ width: `${percentage}%` }}
            onClick={() => {
              handleActionButtonClick(
                setActionData,
                action,
                activeState,
                action,
                dispatch,
              )
            }}
          >
            <div className="float-label">{buttonLabel}</div>
          </div>
        ) : (
          <div
            className="out-of-action"
            onClick={() => {
              handleActionButtonClick(
                setActionData,
                action,
                activeState,
                action,
                dispatch,
              )
            }}
          >
            <div className="float-label">{buttonLabel}</div>
          </div>
        )}
      </div>
    </>
  )
}
