import * as React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'

const nextStep = (percentage: number) => {
  if (percentage === 100) return
  //set hook
  //setState(prevState => ({ percentage: prevState.percentage + 20 }))
}

const ProgressBar = (props: any) => {
  return (
    <div className="progress-bar">
      <Filler percentage={props.percentage} />
    </div>
  )
}

const Filler = (props: any) => {
  return <div className="filler" style={{ width: `${props.percentage}%` }} />
}

export const ProgressBarComponent = () => {
  const [percentage, setPercentage] = useState<number>(50)

  useEffect(() => {
    nextStep(percentage)
  })

  return (
    <div>
      <ProgressBar percentage={percentage} />

      <div
        style={{ marginTop: '3px', color: 'white', marginBottom: '1px' }}
        onClick={() => setPercentage(0)}
      >
        Reset
      </div>
    </div>
  )
}
