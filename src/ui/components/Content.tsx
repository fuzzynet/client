import * as React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { RootState, actions } from 'store'
import * as Types from 'types/ui'
import { createListener } from 'ui/lib'
import { LoadImage } from './LoadImage'

interface Props {}

export const Content: React.FC<Props> = () => {
  const dispatch = useDispatch()

  const {
    content: { _id, height, width } = {} as Partial<Types.Content>,
  } = useSelector((state: RootState) => state.viewContent)

  const keydownEventHandler = (evt: KeyboardEvent) => {
    // TODO: switch for content view keyboard navigation
    if (evt.key === 'Escape') {
      dispatch(actions.viewContent.hideContent())
    }
  }

  React.useEffect(createListener(window, 'keydown', keydownEventHandler))

  return _id && height && width ? (
    <div
      id="content-view"
      onClick={() => {
        dispatch(actions.viewContent.hideContent())
      }}
    >
      <LoadImage hash={_id} height={height} width={width} />
    </div>
  ) : (
    <div>loading...</div>
  )
}
