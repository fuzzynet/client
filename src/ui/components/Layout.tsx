import * as React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { connect } from '@giantmachines/redux-websocket'
import * as _ from 'lodash'
import { useCurrentRoute } from 'react-navi'

import { Nav } from 'components/Nav'
import { Content } from 'components/Content'
import { actions, RootState } from 'store'
import * as Types from 'types/ui'
import { createListener } from 'ui/lib'

const { host, protocol } = window.location

interface Props {
  ws?: WebSocket
}

export const Layout: React.FC<Props> = ({ children }) => {
  const dispatch = useDispatch()
  const { title } = useCurrentRoute()
  const page = title as Types.Routes
  const { shown } = useSelector((state: RootState) => state.viewContent)

  const scrollPosition = useSelector(
    (state: RootState) => state.scrollPosition[page],
  )

  React.useEffect(() => {
    dispatch(connect(`${protocol === 'https:' ? 'wss' : 'ws'}://${host}`))
  }, [protocol, host])

  React.useEffect(() => {
    window.scrollTo(0, scrollPosition)
  })

  const scrollEventHandler = () => {
    dispatch(actions.scrollPosition.update({ page, vertical: window.scrollY }))
  }

  React.useEffect(createListener(window, 'scroll', scrollEventHandler))

  return (
    <>
      <Nav />
      {shown && <Content />}
      <div id="layout">{children}</div>
    </>
  )
}
