import * as React from 'react'

import * as API from 'ui/api/http'
import * as Types from 'types/ui'

const loadingImage = ''

const getContentImage = (
  hash: string,
  src: string,
  setSrc: React.Dispatch<React.SetStateAction<string>>,
) => {
  if (src === loadingImage) {
    const img = new Image()
    const url = API.prefix(Types.routes.get.content(hash))

    img.onerror = () => {
      console.warn('error loading an image, trying again')
      img.src = `${url}?ts=${Date.now()}`
    }

    img.onload = () => {
      setSrc(img.src)
    }

    img.src = url
  }
}

interface Props {
  hash: string
  height: number
  width: number
  className: string
}

export const LoadImage: React.FC<Partial<Props>> = ({
  hash,
  height,
  width,
  className = '',
}) => {
  const [src, setSrc] = React.useState(loadingImage)

  if (hash && hash.length) {
    getContentImage(hash, src, setSrc)
  }

  return src !== loadingImage ? (
    <img src={src} height={height} width={width} className={className} />
  ) : (
    <div style={{ height, width }}>loading...</div>
  )
}
