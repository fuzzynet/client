import * as React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Dispatch } from '@reduxjs/toolkit'
import * as _ from 'lodash'

import * as Types from 'types/ui'
import * as API from 'ui/api/http'
import { smallThumbSize } from 'src/constants'
import { actions, RootState } from 'store'
import { createListener, EventListener } from 'ui/lib'
import { LoadImage } from 'components/LoadImage'

const thumbMargin = 5
const thumbWidth = smallThumbSize + thumbMargin * 2

const feedItemRightClickHandler = (
  _id: string,
  feed: Types.FeedType,
  dispatch: Dispatch,
) => async () => {
  await API.getShareLink(_id)
  dispatch(actions.feedSelection.toggle({ _id, feed }))
}

const feedItemLeftClickHandler = (
  _id: string,
  width: number,
  height: number,
  dispatch: Dispatch,
) => () => {
  dispatch(actions.viewContent.showContent({ _id, width, height }))
}

interface Selected {
  selected: boolean
}

interface Props {
  feedType: Types.FeedType
}

export const FeedItem: React.FC<Types.Content & Selected & Props> = ({
  _id,
  thumb1,
  width,
  height,
  selected,
  feedType,
}) => {
  const dispatch = useDispatch()
  const aspectRatio = height / width
  const thumbHeight = smallThumbSize * aspectRatio

  return (
    <div
      className="feed-item"
      onClick={feedItemLeftClickHandler(_id, width, height, dispatch)}
      onContextMenu={feedItemRightClickHandler(_id, feedType, dispatch)}
    >
      <LoadImage
        hash={thumb1}
        height={thumbHeight}
        width={smallThumbSize}
        className={selected ? 'selected' : ''}
      />
    </div>
  )
}

const incMap = (m: Map<number, number>, i: number, x: number) => {
  m.set(i, (m.get(i) || 0) + x)
}

const fillMap = (m: Map<number, number>, len: number, val: number) => {
  for (let i = 0; i < len; i++) {
    m.set(i, val)
  }
}

const minIndex = (m: Map<number, number>) => {
  const vals = [...m.values()]
  return vals.indexOf(Math.min(...vals))
}

export const Feed: React.FC<Props> = ({ feedType }) => {
  const { innerWidth } = window
  const colCount = Math.floor(innerWidth / thumbWidth)
  const cols: Types.Content[][] = Array.from({ length: colCount + 1 }, () => [])

  const colHeights = new Map<number, number>()
  fillMap(colHeights, colCount, 0)

  const items = useSelector((state: RootState) => state.feedContent[feedType])
  const selections = useSelector(
    (state: RootState) => state.feedSelection[feedType],
  )

  items.forEach((item) => {
    const c = minIndex(colHeights)
    const aspectRatio = item.height / item.width
    const thumbHeight = smallThumbSize * aspectRatio
    incMap(colHeights, c, thumbHeight + thumbMargin * 2)
    cols[c].push(item)
  })

  const [snappedToBottom, setSnappedToBottom] = React.useState(true)

  React.useEffect(() => {
    if (snappedToBottom) {
      window.scrollTo(0, document.body.scrollHeight)
    }
  })

  React.useEffect(
    createListener(window, 'scroll', () => {
      setSnappedToBottom(
        window.scrollY + window.innerHeight >= document.body.scrollHeight,
      )
    }),
    [true],
  )

  return (
    <div className="feed">
      {cols.map((col, i) => {
        return (
          <div key={`feed-column.${col.length}.${i}`} className="feed-column">
            {col.map((item, j) => {
              return (
                <FeedItem
                  key={`feed-item.${item._id}.${j}`}
                  {...item}
                  selected={selections[item._id]}
                  feedType={feedType}
                />
              )
            })}
          </div>
        )
      })}
    </div>
  )
}

export const ResizableFeed: React.FC<Props> = ({ feedType }) => {
  const [width, setWidth] = React.useState(window.innerWidth)

  const resizeEventHandler = _.throttle(
    () => {
      setWidth(window.innerWidth)
    },
    1000,
    { leading: true, trailing: true },
  ) as EventListener

  React.useEffect(createListener(window, 'resize', resizeEventHandler))
  // TODO: memoize if it makes sense to (on route change, for example)

  return <Feed key={`feed-resizer.${width}`} feedType={feedType} />
}
