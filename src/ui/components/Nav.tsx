import * as React from 'react'
import { Link } from 'react-navi'
import { useSelector } from 'react-redux'

import { RootState } from 'store'

interface Props {}

export const Nav: React.FC<Props> = () => {
  const shareStatus = useSelector((state: RootState) => state.shareStatus)

  return (
    <div id="nav">
      <div className="left">
        <Link activeClassName="active" href="/">
          <img src="/static/favicon.png" width="64" height="64" />
        </Link>
        <Link activeClassName="active" href="/share">
          <div>share</div>
          {/* TODO: make this vertical column aligned */}
          {shareStatus.active && (
            <div>
              {shareStatus.count} / {shareStatus.total}
            </div>
          )}
        </Link>
        <div className="disabled">discover</div>
        <div className="disabled">curate</div>
        <div className="disabled">mine</div>
        <div className="disabled">connect</div>
        {/* <Link activeClassName="active" href="/discover">
          discover
        </Link><Link activeClassName="active" href="/curate">
          curate
        </Link>
        <Link activeClassName="active" href="/mine">
          mine
        </Link>
        <Link activeClassName="active" href="/mobile">
          connect
        </Link> */}
      </div>
      <div className="right">
        <div className="disabled">submit</div>
        <div className="disabled">updates</div>
        <div className="disabled">help</div>
        {/* <Link activeClassName="active" href="/submit">
          submit
          <br />
          {changeCount}
        </Link>
        <Link activeClassName="active" href="/updates">
          updates
          <br />
          {updateCount}
        </Link>
        <Link activeClassName="active" href="/help">
          {helpRoute}
          <br />
          help
        </Link> */}
      </div>
    </div>
  )
}
