import { homedir } from 'os'
import * as path from 'path'

import { expressApp } from 'api'
import * as data from 'data'
import { ports } from 'src/constants'
import Future from 'lib/future'
import * as Types from 'types/api'

const init = async () => {
  try {
    const folder = path.join(homedir(), '.fuzzynet')

    const resourcesFuture = new Future<Types.Resources>()
    const reduxLoadedFuture = new Future<void>()

    const expressAppInit = expressApp(
      ports,
      __dirname,
      resourcesFuture,
      reduxLoadedFuture,
    )

    const resources = await data.init(folder, __dirname, reduxLoadedFuture)

    resourcesFuture.resolve(resources)

    await expressAppInit(resources)
    console.info(
      `express app now running in headless mode on port ${resources.ports.http}`,
    )
  } catch (err) {
    console.error(err)
  }
}

init()
