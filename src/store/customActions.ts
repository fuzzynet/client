import * as Types from 'types/shared'

export const handlers: Types.KV<(data: unknown) => void> = {
  // to force a reload on the ui from the api
  reload: () => {
    window.location.reload()
  },
}

export const bypassRedux = (method = '') => `bypassRedux/${method}`

export const createAction = (method: string, _data?: any) =>
  JSON.stringify({ type: bypassRedux(method) })

export const creators = {
  reload: () => createAction('reload'),
}
