import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import * as Types from 'types/shared'

interface ViewContent {
  shown: boolean
  content?: Partial<Types.Content>
}

const initialState: ViewContent = {
  shown: false,
  content: undefined,
}

export const viewContent = createSlice({
  name: 'viewContent',
  reducers: {
    showContent: (state, action: PayloadAction<Partial<Types.Content>>) => {
      state.shown = true
      state.content = action.payload
      return state
    },
    hideContent: (state) => {
      state.shown = false
      state.content = undefined
      return state
    },
  },
  initialState,
})

export default viewContent
