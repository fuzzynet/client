import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import * as Types from 'types/shared'

interface SelectionKV {
  [_id: string]: boolean
}

interface FeedSelection {
  personal: SelectionKV
  external: SelectionKV
}

const initialState: FeedSelection = {
  personal: {},
  external: {},
}

interface FeedSelectionAction {
  feed: Types.FeedType
  _id: string
}

export const feedSelection = createSlice({
  name: 'feedSelection',
  reducers: {
    select: (state, action: PayloadAction<FeedSelectionAction>) => {
      const { _id, feed } = action.payload
      state[feed][_id] = true
      return state
    },
    deselect: (state, action: PayloadAction<FeedSelectionAction>) => {
      const { _id, feed } = action.payload
      state[feed][_id] = false
      return state
    },
    toggle: (state, action: PayloadAction<FeedSelectionAction>) => {
      const { _id, feed } = action.payload
      state[feed][_id] = !state[feed][_id]
      return state
    },
  },
  initialState,
})

export default feedSelection
