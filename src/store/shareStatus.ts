import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface ShareStatus {
  active: boolean
  count: number
  total: number
}

const initialState: ShareStatus = {
  active: false,
  count: 0,
  total: 0,
}

export const shareStatus = createSlice({
  name: 'shareStatus',
  reducers: {
    setActive: (state, action: PayloadAction<boolean>) => {
      state.active = action.payload
      return state
    },
    setCount: (state, action: PayloadAction<number>) => {
      state.count = action.payload
      return state
    },
    setTotal: (state, action: PayloadAction<number>) => {
      state.total = action.payload
      return state
    },
  },
  initialState,
})

export default shareStatus
