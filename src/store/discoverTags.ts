import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface DiscoverTags {
  active: boolean
  showTags: boolean
}

const initialState: DiscoverTags = {
  active: false,
  showTags: true,
}

export const discoverTags = createSlice({
  name: 'discoverTags',
  reducers: {
    setActive: (state, action: PayloadAction<boolean>) => {
      state.active = action.payload
      return state
    },
    setShowTags: (state, action: PayloadAction<boolean>) => {
      state.showTags = action.payload
      return state
    },
  },
  initialState,
})

export default discoverTags
