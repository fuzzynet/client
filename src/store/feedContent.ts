import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import * as Types from 'types/shared'

export interface FeedContent {
  personal: Types.Content[]
  external: Types.Content[]
}

const initialState: FeedContent = {
  personal: [],
  external: [],
}

export const feedContent = createSlice({
  name: 'feedContent',
  reducers: {
    addPersonal: (state, action: PayloadAction<Types.Content>) => {
      state.personal.push(action.payload)
      return state
    },
    addExternal: (state, action: PayloadAction<Types.Content>) => {
      state.external.push(action.payload)
      return state
    },
  },
  initialState,
})

export default feedContent
