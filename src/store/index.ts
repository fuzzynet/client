import * as _ from 'lodash'
import { Store } from 'redux'
import {
  configureStore,
  getDefaultMiddleware,
  Middleware,
  combineReducers,
} from '@reduxjs/toolkit'
import { reduxBatch } from '@manaflair/redux-batch'

import { viewContent } from 'store/viewContent'
import { feedContent } from 'store/feedContent'
import { feedSelection } from 'store/feedSelection'
import { shareStatus } from 'store/shareStatus'
import { discoverTags } from 'store/discoverTags'
import { scrollPosition } from 'store/scrollPosition'

const enhancers = [reduxBatch as any] // TODO: w/e, bug fix, not our code

const rootReducer = combineReducers({
  viewContent: viewContent.reducer,
  feedContent: feedContent.reducer,
  feedSelection: feedSelection.reducer,
  shareStatus: shareStatus.reducer,
  discoverTags: discoverTags.reducer,
  scrollPosition: scrollPosition.reducer,
})

export type RootState = ReturnType<typeof rootReducer>
export type ReduxStore = Store<RootState>

export const actions = {
  viewContent: viewContent.actions,
  feedContent: feedContent.actions,
  feedSelection: feedSelection.actions,
  shareStatus: shareStatus.actions,
  discoverTags: discoverTags.actions,
  scrollPosition: scrollPosition.actions,
}

interface StoreOptions {
  preloadedState: Partial<RootState>
  prependedMiddleware: Middleware[]
  appendedMiddleware: Middleware[]
}

export default (options: Partial<StoreOptions> = {}): ReduxStore => {
  const {
    preloadedState = {},
    prependedMiddleware = [],
    appendedMiddleware = [],
  } = options

  const middleware = [
    ...prependedMiddleware,
    ...getDefaultMiddleware(),
    ...appendedMiddleware,
  ]

  return configureStore({
    reducer: rootReducer,
    middleware,
    enhancers,
    devTools: process.env.NODE_ENV !== 'production',
    preloadedState,
  })
}
