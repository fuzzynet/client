import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import * as Types from 'types/shared'

type ScrollPosition = {
  [Page in Types.Routes]: number
}

interface ScrollPositionAction {
  page: Types.Routes
  vertical: number
}

const initialState: ScrollPosition = {
  home: 0,
  mobile: 0,
  discover: 0,
  curate: 0,
  share: 0,
  mine: 0,
  account: 0,
  help: 0,
  switch: 0,
}

export const scrollPosition = createSlice({
  name: 'scrollPosition',
  reducers: {
    update: (
      state: ScrollPosition,
      action: PayloadAction<ScrollPositionAction>,
    ) => {
      const { page, vertical } = action.payload
      state[page] = vertical
      return state
    },
  },
  initialState,
})

export default scrollPosition
