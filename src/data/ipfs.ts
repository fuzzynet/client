// for anyone reading this file, just know, the ipfs project is a complete goddamned shit show and this is what we had to do.
// you have been warned.
const Ctl = require('ipfsd-ctl')
// import * as getPort from 'get-port'
import * as path from 'path'
import { promises as fs } from 'fs'

import * as Types from 'types/api'
import { ipfsOptions } from 'data/ipfs.config'

const sleep = (millis: number) =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve()
    }, millis)
  })

const spawn = async (
  init: boolean,
  repo: string,
  port: number,
): Promise<Types.IPFSD> => {
  let node: Types.IPFSD | undefined = undefined

  process.env.IPFS_PATH = repo
  ipfsOptions.init = init
  ipfsOptions.start = true
  ipfsOptions.repo = repo

  // const ciArgs = process.env.CI ? ['--offline'] : []
  node = await Ctl.createFactory({
    remote: false,
    type: 'go',
    ipfsOptions,
    disposable: false,
    port,
  }).spawn({
    repo,
    init,
    start: true,
    disposable: false,
    args: ['--enable-pubsub-experiment', '--debug' /*...ciArgs*/],
    config: ipfsOptions,
    port,
  })

  if (!node) {
    console.error('no ipfs btw...')
    throw new Error('no ipfs')
  } else {
    if (init) {
      await node.init()
    }

    await node.start()
    await node.ready

    return node
  }
}

export const init = async (
  store: Types.Snap,
  folder: string,
  ports: Types.Ports,
): Promise<Types.IPFSD> => {
  let ipfsd: Types.IPFSD | undefined
  let retry = false
  const repo = path.resolve(folder, 'ipfs')
  let port: number
  let resetLock = false

  if (ports.ipfs) {
    port = ports.ipfs
    console.info('reusing ipfs port:', port)
  } else {
    // port = await getPort()
    port = 5001
    ports.ipfs = port
    console.info('getting new ipfs port:', port)
  }

  try {
    const hasIpfsInit = await store.get<boolean>('ipfs.init', false)
    ipfsd = await spawn(hasIpfsInit === false, repo, port)

    if (ipfsd && ipfsd.error) {
      console.warn('problem spawning ipfs daemon', ipfsd)
      retry = true
    } else {
      retry = false
    }
  } catch (err) {
    console.warn('ipfs fail #1', err)
    retry = true
  }

  if (retry || !ipfsd || (ipfsd && ipfsd.error)) {
    try {
      ipfsd = await spawn(false, repo, port)

      if (ipfsd && ipfsd.error) {
        console.warn('problem spawning ipfs daemon', ipfsd)
        retry = true
      } else {
        retry = false
      }
    } catch (err) {
      console.warn('ipfs fail #2')
      retry = true
    }
  }

  if (retry) {
    try {
      ipfsd = await spawn(true, repo, port)
      retry = false
    } catch (err) {
      console.error('ipfs fail #3')
      resetLock = true
    }
  }

  if (resetLock) {
    console.warn('ipfs shitting itself, waiting a bit and trying again')
    await sleep(5000)
    try {
      await Promise.all([
        fs.unlink(path.join(repo, 'api')),
        fs.unlink(path.join(repo, 'repo.lock')),
      ])
    } catch (err) {
      console.warn('nothing here')
    }
    ipfsd = await init(store, folder, ports)
  }

  if (!ipfsd || (ipfsd && ipfsd.error)) {
    throw new Error(`ipfs not initialized... was ${!retry && 'not'} retried`)
  }

  await store.set('ipfs.init', true)

  return ipfsd
}

// try { // TODO: cleanup and put on stats on account settings screen
//   const { id } = await ipfsd.api.id()
//   console.info('ipfs id', id)
// } catch (err) {
//   console.error(err)
// }
