import * as path from 'path'
import * as os from 'os'

// import OrbitDB from 'orbit-db'
const OrbitDB = require('orbit-db') // TODO: better typing for orbit-db after quick and dirty fix here
const Identities = require('orbit-db-identity-provider')
import { SnapDB } from 'snap-db'
import * as WebSocket from 'ws'

import { ports } from 'src/constants'
import * as Types from 'types/api'
import * as ipfs from 'data/ipfs'
import * as Model from 'data/model'
import * as Crypto from 'lib/crypto'
import { Snap } from 'data/snap'
import * as Redux from 'data/redux'
import Future from 'lib/future'

export const init = async (
  folder: string,
  appPath: string,
  reduxLoadedFuture: Future<void>,
): Promise<Types.Resources> => {
  let resources: Types.Resources

  // TODO: multi-account
  try {
    // k/v db
    const snap = new SnapDB<string>(path.resolve(folder, 'snapdb'))
    const store = new Snap(snap)

    // auth
    const crypto = await Crypto.init()
    const account = await store.get<string>(
      'account.default',
      crypto.account,
      true,
    )
    crypto.account = account

    // ipfs & orbitdb
    const ipfsd = await ipfs.init(store, folder, ports)
    const identity = await Identities.createIdentity({
      id: account,
      identityKeysPath: path.resolve(folder, 'identity'),
    })
    const orbitdb = await OrbitDB.createInstance(ipfsd.api, {
      directory: path.resolve(folder, 'orbitdb'),
      identity,
    })
    const model = await Model.init(orbitdb, store, account)

    // websockets
    const wsUser = new Map<string, WebSocket>()
    const wsAccount = new Map<string, string[]>()
    const wsBroadcast = (account: string, action: string) => {
      wsAccount.get(account)?.forEach((user) => {
        wsUser.get(user)?.send(action)
      })
    }

    // redux
    const redux = await Redux.init(
      model,
      wsBroadcast,
      account,
      reduxLoadedFuture,
    )

    // TODO: multi-account
    const dispatch = async (
      action: Types.ActionEvent,
      _user: string,
    ): Promise<void> => {
      // send to ui
      // TODO: fix for 0.1.1 (mobile)
      // const socket = wsUser.get(user)
      // if (socket) {
      //   socket.send(JSON.stringify(action))
      // }
      wsBroadcast(account, JSON.stringify(action))

      // record in redux in-memory
      redux.dispatch(action)

      // log to orbitdb
      if (model.log) {
        await model.log.add(action)
      } else {
        console.error('no log found for action', action)
      }
    }

    resources = {
      cpus: os.cpus().length,
      ports,
      folder,
      appPath,
      store,
      ipfs: ipfsd.api,
      orbitdb,
      model,
      wsUser,
      wsAccount,
      wsBroadcast,
      crypto,
      redux,
      dispatch,
    }
  } catch (err) {
    console.error(err)
    throw new Error(err)
  }

  return resources
}
