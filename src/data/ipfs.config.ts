// import * as IPFS from 'ipfs' // wtf, IPFS... no options config types? you fucking suck.

export const ipfsOptions: any = {
  API: {
    HTTPHeaders: {
      'Access-Control-Allow-Methods': ['PUT', 'POST', 'GET'],
      'Access-Control-Allow-Origin': [
        'http://localhost:3000',
        'https://webui.ipfs.io',
      ],
    },
  },
  Addresses: {
    API: '/ip4/127.0.0.1/tcp/5001',
    Announce: [],
    Gateway: '/ip4/127.0.0.1/tcp/8080',
    NoAnnounce: [],
    Swarm: ['/ip4/127.0.0.1/tcp/4001'],
  },
}
