import { SnapDB } from 'snap-db'
import * as Types from 'types/api'

// TODO: schema validation and migration
// TODO: batch operations
export class Snap {
  private snap: SnapDB<string>

  constructor(snap: SnapDB<string>) {
    this.snap = snap
  }

  public async get<T>(key: string): Promise<Types.Maybe<T>>
  public async get<T>(key: string, defaultValue: T): Promise<T>
  public async get<T>(
    key: string,
    defaultValue: T,
    persist: boolean,
  ): Promise<T>
  public async get<T>(
    key: string,
    defaultValue?: T,
    persist?: boolean,
  ): Promise<T | undefined> {
    const result = await this.snap.get(key)
    if (result) {
      return JSON.parse(result)
    } else if (defaultValue) {
      if (persist) {
        await this.set(key, defaultValue)
      }
      return defaultValue
    } else {
      return undefined
    }
  }

  public async iterate<T>(name = ''): Promise<Types.KV<T>> {
    const data = await this.snap.queryIt({ gt: name, lt: name + '~' })
    const result: string[][] = []
    for await (const [key, value] of data) {
      result.push([key.replace(name, ''), JSON.parse(value)])
    }
    return Object.fromEntries(result)
  }

  public async set(key: string, value: any): Promise<void> {
    await this.snap.put(key, JSON.stringify(value))
  }
}
