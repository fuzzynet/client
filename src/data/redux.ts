import { Middleware } from '@reduxjs/toolkit'

import Store, { ReduxStore } from 'store'
import * as customActions from 'store/customActions'
import * as Types from 'types/api'
import { forEach } from 'data/orbitdb'
import { logger } from 'lib/logger'
import Future from 'lib/future'

export const futureCounter = (
  reduxLoadedFuture: Future<void>,
  expectedCountFuture: Future<number>,
): Middleware => (store: any) => (next) => async (action) => {
  const { _counter = 0 } = store
  store._counter = _counter + 1
  const expectedCount = await expectedCountFuture
  if (expectedCount !== 0 && store._counter === expectedCount) {
    reduxLoadedFuture.resolve()
  }
  next(action)
}

export const init = async (
  model: Types.Model,
  wsBroadcast: Types.WSBroadcast,
  account: string,
  reduxLoadedFuture: Future<void>,
): Promise<ReduxStore> => {
  const countFuture = new Future<number>()

  const redux = Store({
    appendedMiddleware: [futureCounter(reduxLoadedFuture, countFuture)],
  })

  const count = await forEach<Types.ActionEvent>(model.log, {
    eventHook: (evt: any) => redux.dispatch(logger.log(evt)),
    doneHook: (_count: number) => {
      wsBroadcast(account, customActions.creators.reload())
    },
    schemaValidator: Types.ActionEvent,
  })

  if (count === 0) {
    reduxLoadedFuture.resolve()
  }

  countFuture.resolve(count)

  return redux
}
