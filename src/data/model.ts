import OrbitDB from 'orbit-db'
import * as _ from 'lodash'

import * as Types from 'types/api'

interface DBs {
  localAccount: Types.OrbitDocumentStore<Types.Account>
  peerAccount: Types.OrbitDocumentStore<Types.Account>
  content: Types.OrbitDocumentStore<Types.Content>
  tags: Types.OrbitDocumentStore<Types.Tag>
  subscriptions: Types.OrbitDocumentStore<Types.Subscription>
  personal: Types.OrbitFeedStore<Types.ID>
  external: Types.OrbitFeedStore<Types.ID>
  log: Types.OrbitLogStore<Types.ActionEvent>
}

type DBArray = [
  Types.OrbitDocumentStore<Types.Account>,
  Types.OrbitDocumentStore<Types.Account>,
  Types.OrbitDocumentStore<Types.Content>,
  Types.OrbitDocumentStore<Types.Tag>,
  Types.OrbitDocumentStore<Types.Subscription>,
  Types.OrbitFeedStore<Types.ID>,
  Types.OrbitFeedStore<Types.ID>,
  Types.OrbitLogStore<Types.ActionEvent>,
]

export const init = async (
  orbitdb: OrbitDB,
  snap: Types.Snap,
  account: string,
): Promise<Types.Model> => {
  let model: Types.Model

  const map = ([
    localAccount,
    peerAccount,
    content,
    tags,
    subscriptions,
    personal,
    external,
    log,
  ]: DBArray): DBs => ({
    localAccount,
    peerAccount,
    content,
    tags,
    subscriptions,
    personal,
    external,
    log,
  })

  const unmap = ({
    localAccount,
    peerAccount,
    content,
    tags,
    subscriptions,
    personal,
    external,
    log,
  }: DBs): DBArray => [
    localAccount,
    peerAccount,
    content,
    tags,
    subscriptions,
    personal,
    external,
    log,
  ]

  const names = [
    'localAccount',
    'peerAccount',
    'content',
    'tags',
    'subscriptions',
    'personal',
    'external',
    'log',
  ]

  const save = async (dbs: DBs) => {
    const addresses = unmap(dbs).map(({ address }) => address.toString())
    await Promise.all(
      _.zip(names, addresses).map(([name, address]) =>
        snap.set(`orbitdb.${account}.${name}`, address),
      ),
    )
    return dbs
  }

  const load = async () => {
    return await snap.iterate<string>(`orbitdb.${account}.`)
  }

  const create = async (): Promise<DBs> => {
    const dbs = await Promise.all([
      orbitdb.docs<Types.Doc<Types.Account>>('account.local'),
      orbitdb.docs<Types.Doc<Types.Account>>('account.peers'),
      orbitdb.docs<Types.Doc<Types.Content>>('content'),
      orbitdb.docs<Types.Doc<Types.Tag>>('tags'),
      orbitdb.docs<Types.Doc<Types.Subscription>>('subscriptions'),
      orbitdb.feed<Types.ID>('feed.personal'),
      orbitdb.feed<Types.ID>('feed.external'),
      orbitdb.log<Types.ActionEvent>('log'),
    ])

    await snap.set(`account.${account}.initialized`, true)

    return await save(map(dbs))
  }

  const reload = async () => {
    const address = await load()

    const dbs = await Promise.all([
      orbitdb.docs<Types.Doc<Types.Account>>(address.localAccount),
      orbitdb.docs<Types.Doc<Types.Account>>(address.peerAccount),
      orbitdb.docs<Types.Doc<Types.Content>>(address.content),
      orbitdb.docs<Types.Doc<Types.Tag>>(address.tags),
      orbitdb.docs<Types.Doc<Types.Subscription>>(address.subscriptions),
      orbitdb.feed<Types.ID>(address.personal),
      orbitdb.feed<Types.ID>(address.external),
      orbitdb.log<Types.ActionEvent>(address.log),
    ])

    await Promise.all(dbs.map((db) => db.load()))

    return map(dbs)
  }

  const databases = async () => {
    if (await snap.get<boolean>(`account.${account}.initialized`)) {
      return await reload()
    } else {
      return await create()
    }
  }

  try {
    const {
      localAccount,
      peerAccount,
      content,
      tags,
      subscriptions,
      personal,
      external,
      log,
    } = await databases()

    model = {
      account: {
        local: localAccount,
        peers: peerAccount,
      },
      content,
      tags,
      subscriptions,
      feeds: { personal, external },
      log,
    }
  } catch (err) {
    console.error(err)
    throw new Error(err)
  }

  return model
}
