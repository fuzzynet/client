import * as Types from 'types/api'
import * as t from 'io-ts'

type EventHook<T> = (evt: T) => void

interface OrbitDBIteratorOptions {
  gt: string
  gte: string
  lt: string
  lte: string
  limit: number
  reverse: boolean
}

interface IterateOptions<T> {
  eventHook: EventHook<T>
  doneHook: EventHook<number>
  orbitOptions: Partial<OrbitDBIteratorOptions>
  schemaValidator: t.TypeOf<any>
}

export const forEach = async <T>(
  model: Types.IterableOrbitStore<T>,
  options: Partial<IterateOptions<T>> = {},
) => {
  const {
    eventHook,
    doneHook,
    orbitOptions = { limit: -1 },
    schemaValidator,
  } = options

  try {
    await model.load()
    const iterator = model.iterator(orbitOptions)

    let count = 0
    let done = false

    while (!done) {
      const result = iterator.next()
      const payload = result?.value?.payload?.value
      done = result.done
      if (schemaValidator && !schemaValidator.is(payload)) {
        console.warn('iterated content does not match schema', result)
        continue
      }
      count++
      if (eventHook) {
        eventHook(payload)
      }
    }

    await model.close()

    console.info(count, 'items processed')

    if (doneHook) {
      doneHook(count)
    }

    return count
  } catch (err) {
    console.error('orbitdb iteration error', err)
    throw new Error(err)
  }
}

interface UnknownRecord extends Types.RecordID {
  [k: string]: unknown
}

type Record<T> = UnknownRecord & T

const compare = <T>(query: Partial<T>, either: boolean) => (doc: Record<T>) => {
  const queries = Object.entries(query)
  for (let [key, val] of queries) {
    if (either) {
      if (doc[key] === val) {
        return true
      }
    } else {
      if (doc[key] !== val) {
        return false
      }
    }
  }
  if (either) {
    return false
  } else {
    return true
  }
}

export const filter = async <T>(
  model: Types.OrbitDocumentStore<T>,
  query: Partial<Types.Doc<T>>,
  either = false,
): Promise<Types.Doc<T>[]> => {
  return model.query(compare<Record<T>>(query, either))
}
