import * as path from 'path'

import { app, BrowserWindow, nativeImage, shell } from 'electron'
import * as _ from 'lodash'
import * as log from 'electron-log'
import { SnapDB } from 'snap-db'

const folder = path.resolve(app.getPath('home'), '.fuzzynet')
app.setAppLogsPath(path.resolve(folder, 'logs'))

import * as ipfs from 'data/ipfs'
import { expressApp } from 'api'
import * as data from 'data'
import { ports } from 'src/constants'
import Future from 'lib/future'
import * as Types from 'types/api'
import { Snap } from 'data/snap'

log.transports.file.resolvePath = ({
  electronDefaultDir = '',
  fileName = '',
}) => path.resolve(electronDefaultDir, fileName)

let resources: Types.Resources

const arrSearch = (term: string, arr: any[]) => {
  for (let item of arr) {
    if (item.toString().includes(term)) {
      return true
    }
  }
  return false
}

let isIPFSHandlingAnError = false

const recoverIPFS = async () => {
  let store = resources.store

  if (!store) {
    const snap = new SnapDB<string>(path.resolve(folder, 'snapdb'))
    store = new Snap(snap)
  }

  const ipfsd = await ipfs.init(store, folder, ports)

  if (typeof resources !== 'object') {
    // @ts-ignore
    resources = {}
  }

  resources.ipfs = ipfsd.api
  isIPFSHandlingAnError = false
}

const interceptErrors = _.mapValues(log.functions, (func) => {
  return async (...args: any[]) => {
    func(...args)

    // handle ipfs crash (slightly) more gracefully...
    if (arrSearch('ECONNREFUSED', args)) {
      if (isIPFSHandlingAnError) {
        console.info('ipfs is being dealt with... standing by...')
      } else {
        console.warn('ipfs daemon crashed, attempting to restart')
        isIPFSHandlingAnError = true

        recoverIPFS()

        setTimeout(() => {
          if (isIPFSHandlingAnError) {
            recoverIPFS()
          }
        }, 30000)

        console.info('ipfs daemon recovered successfully')
      }
    }
  }
})

Object.assign(console, interceptErrors)

let mainWindow: Electron.BrowserWindow

const icon = path.resolve(__dirname, 'static', 'favicon.png')

if (process.env.CI) {
  app.commandLine.appendSwitch('headless')
  app.commandLine.appendSwitch('disable-gpu')
}

app.commandLine.appendSwitch('disable-renderer-backgrounding')

const createWindow = async () => {
  mainWindow = new BrowserWindow({
    icon,
    frame: false,
    title: 'fuzzynet',
    backgroundColor: '#171919', // fuzzblak
    webPreferences: {
      devTools: true,
      nodeIntegration: false,
      backgroundThrottling: false,
    },
  })

  // TODO: restore window size on resize
  // mainWindow.maximize()
  mainWindow.setSize(1920, 1080, false)
  mainWindow.setPosition(0, 0, false)

  mainWindow.setIcon(nativeImage.createFromPath(icon))

  const appPath = app.getAppPath().replace(/\/app.asar$/, '')

  const resourcesFuture = new Future<Types.Resources>()
  const reduxLoadedFuture = new Future<void>()

  const expressAppInit = expressApp(
    ports,
    appPath,
    resourcesFuture,
    reduxLoadedFuture,
  )

  await mainWindow.loadURL(`http://127.0.0.1:${ports.http}/`)

  resources = await data.init(folder, appPath, reduxLoadedFuture)

  resourcesFuture.resolve(resources)

  await expressAppInit(resources)

  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  // mainWindow.on('closed', () => {
  //   // Dereference the window object, usually you would store windows
  //   // in an array if your app supports multi windows, this is the time
  //   // when you should delete the corresponding element.
  //   mainWindow = null
  // })

  // Handles opening URL in local browser in Electron app
  mainWindow.webContents.on('new-window', async (event, url) => {
    event.preventDefault()
    await shell.openExternal(url)
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// TODO: doesn't work, promises
// app.on('before-quit', async (evt) => {
//   evt.preventDefault()
//   await resources.ipfs.stop()
//   app.quit()
// })
