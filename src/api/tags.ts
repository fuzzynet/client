import { Router, Request, Response } from 'express'
import * as _ from 'lodash'

import * as Types from 'types/api'

export const tags = (_resources: Types.Resources) => {
  const router = Router()

  router.post(Types.routes.post.tags(), async (req: Request, res: Response) => {
    const input: Types.PostTagsRequest = req.body
    const output: Types.PostTagsResponse = input.map((s: string) =>
      _.snakeCase(s.trim()),
    )
    res.json(output)
  })

  // router.get(Types.routes.get.tags(':hash'), (req: Request, res: Response) => {
  //   const { hash } = req.params
  // })

  return router
}
