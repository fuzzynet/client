import * as path from 'path'

import { Router, Request, Response } from 'express'

import * as Types from 'types/api'

export const account = (resources: Types.Resources) => {
  const router = Router()

  // router.get('/account', async (req: Request, res: Response) => {
  //   const {} = req.body
  // })

  router.post(
    Types.routes.post.account.new(),
    async (_req: Request, res: Response) => {
      try {
        const id = 'f00f' // TODO: await get id from somewhere

        const accountCard = path.join(
          resources.folder,
          'account',
          `account_card_${id}.png`,
        )
        res.download(accountCard, `fuzzynet account card for ${id}.png`)
      } catch (err) {
        console.error(err)
        res.sendStatus(500)
      }
    },
  )

  return router
}
