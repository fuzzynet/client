import { promises as fs } from 'fs'
import * as assert from 'assert'
import * as path from 'path'

import { Router, Request, Response } from 'express'
import * as _ from 'lodash'
import * as fileType from 'file-type'
import readChunk from 'read-chunk'
import * as sharp from 'sharp'

import * as Types from 'types/api'
import * as Crypto from 'lib/crypto'
import { smallThumbSize, largeThumbSize } from 'src/constants'
import { actions } from 'store'

const ipfsOptions = {
  pin: true, // TODO: pin by default... turn off when it makes sense to?
  // progress: // TODO progress loader
}

// persistent file walk state
const files: string[] = []
let i = 0
let done = false

const store = async (
  resources: Types.Resources,
  user: string,
  type: string,
  original: Buffer,
  width: number,
  height: number,
  length: number,
): Promise<boolean> => {
  const [[{ hash: _id }], smallThumb, largeThumb] = await Promise.all([
    // check hash against content lookup
    resources.ipfs.add(original, {
      onlyHash: true,
    }),
    // thumbnail
    sharp(original)
      .resize(smallThumbSize)
      .toFormat(sharp.format.webp)
      .toBuffer(),

    sharp(original)
      .resize(largeThumbSize)
      .toFormat(sharp.format.webp)
      .toBuffer(),
  ])

  const [existingRecord] = resources.model.content.get(_id)

  if (Types.Content.is(existingRecord)) {
    // skipped
    // TODO: communicate item to frontend if necessary (after feed can be cleared)
    return true
  }

  // add to ipfs
  const [
    [{ hash: content }],
    [{ hash: thumb1 }],
    [{ hash: thumb2 }],
  ] = await Promise.all([
    resources.ipfs.add(original, ipfsOptions),
    resources.ipfs.add(smallThumb, ipfsOptions),
    resources.ipfs.add(largeThumb, ipfsOptions),
  ])

  assert(content === _id, 'content hash does not match lookup hash')

  const record: Types.Content = {
    _id,
    type,
    thumb1,
    thumb2,
    width,
    height,
    length,
  }

  console.info('content added to ipfs', record)

  // add record to personal feed and content reverse lookup
  await Promise.all([
    resources.model.content.put(record),
    resources.model.feeds.personal.add(content),
  ])

  // dispatch event
  resources.dispatch(actions.feedContent.addPersonal(record), user)

  console.info('content added to orbitdb')

  // not skipped
  return false
}

export const content = (resources: Types.Resources) => {
  const router = Router()

  router.post(
    Types.routes.post.content(),
    async (req: Request, res: Response) => {
      const { user } = await Crypto.token(resources, req, res)

      resources.dispatch(actions.shareStatus.setActive(true), user)

      // typed
      const input = req.body as Types.PostContentRequest

      console.info('files input', input)

      // ok
      res.sendStatus(200)

      // types (no gif, svg, tiff, or heif/heic yet)
      const imageTypes = ['png', 'jpg', 'webp']
      const videoTypes = [
        'apng',
        'avi',
        'flv',
        'h264',
        'hevc',
        'm4v',
        'mkv',
        'mov',
        'mp4',
        'mpeg',
        'webm',
      ]
      // TODO: after seleting an audio encoder, select audio formats
      // const audioTypes = []
      // TODO: same for text files
      // const textTypes = []

      done = false

      // walk
      // TODO: timeout will make this more resilient
      while (!done && input.length) {
        const file = input[i]
        try {
          const stat = await fs.stat(file)

          if (stat.isDirectory()) {
            const dir = await fs.readdir(file)
            input.push(...dir.map((fd) => path.join(file, fd))) // should work for 200k+ files
          } else {
            files.push(file)
          }
        } catch (err) {
          console.error(err)
          // TODO: send error event to ui
          done = true
        }

        i++

        if (i === input.length) {
          done = true
        }
      }

      console.info('files found', input)

      resources.dispatch(actions.shareStatus.setTotal(files.length), user)

      // type
      let count = 1
      for (let file of files) {
        const type = fileType(await readChunk(file, 0, fileType.minimumBytes))

        if (!type) {
          console.error('no type could be found for', file)
          continue
        }

        // images (sharp)
        if (imageTypes.includes(type.ext)) {
          try {
            const original = sharp(file)

            const { width = 0, height = 0 } = await original.metadata()

            const content = await original
              .rotate()
              .toFormat(sharp.format.webp)
              .toBuffer()

            const skipped = await store(
              resources,
              user,
              'image',
              content,
              width,
              height,
              width * height,
            )

            if (skipped) {
              console.info('skipped image', file, 'for', user)
            } else {
              console.info('processed image', file, 'for', user)
            }
          } catch (err) {
            if (err.toString().includes('ECONNREFUSED')) {
              await resources.ipfs.start()
              // TODO: fork their shitty ipfs types and fix that shit
              // @ts-ignore
              await resources.ipfs.ready
            } else {
              console.error('error processing image', type, file, err)
              continue
            }
          }
        }
        // gifs (animated or still)
        else if (type.ext === 'gif') {
          // TODO: animated gif detection
          console.info('gif not supported yet:', file)
        }
        // videos (ffmpeg)
        else if (videoTypes.includes(type.ext)) {
        }
        // TODO: musics (?)
        // TODO: texts
        // unsupported
        else {
          console.info('unsupported format:', type, 'for file:', file)
          // TODO: send unsupported type error to ui
          continue
        }

        count++

        resources.dispatch(actions.shareStatus.setCount(count), user)
      }

      resources.dispatch(actions.shareStatus.setActive(false), user)
    },
  )

  router.get(
    Types.routes.get.content(':hash'),
    (req: Request, res: Response) => {
      const { hash } = req.params
      const { accept } = req.headers

      if (accept) {
        res.type(accept)
      }

      const filesStream = resources.ipfs.getReadableStream(hash)

      filesStream.on('data', ({ content }: any) => {
        content.pipe(res)
      })
    },
  )

  return router
}
