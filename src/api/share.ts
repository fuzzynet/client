import { clipboard } from 'electron'
import { Router, Request, Response } from 'express'
import * as _ from 'lodash'
const codec = require('json-url')('lzw')

import * as Crypto from 'lib/crypto'
import * as Types from 'types/api'
import { actions } from 'store'
import { forwarder } from 'src/constants'
import { streamAwait } from 'lib/stream'

export const share = (resources: Types.Resources) => {
  const router = Router()

  // router.post(Types.routes.post.share(), (req: Request, res: Response) => {
  //   res.json(output)
  // })

  router.get(
    Types.routes.get.share(':content'),
    async (req: Request, res: Response) => {
      try {
        const { content } = req.params
        const [record] = resources.model.content.get(content)
        if (!record) {
          res.sendStatus(404)
        } else {
          const code = await codec.compress(record)
          const link = `${forwarder}/${code}`
          clipboard.writeText(link)
          res.send(link)
        }
      } catch (err) {
        console.error(err)
      }
    },
  )

  router.get(
    Types.routes.get.open(':content'),
    async (req: Request, res: Response) => {
      const { content } = req.params
      const { user } = await Crypto.token(resources, req, res)
      const record = await codec.decompress(content)

      if (Types.Content.is(record)) {
        const [existingContent, externalContent] = await Promise.all([
          resources.model.content.get(record._id),
          resources.model.feeds.external.get(record._id),
        ])

        const promises: any[] = []

        if (!existingContent) {
          promises.push(resources.model.content.put(record))
          promises.push(
            streamAwait(resources.ipfs.getReadableStream(record._id)),
          )
          promises.push(
            streamAwait(resources.ipfs.getReadableStream(record.thumb1)),
          )
          promises.push(
            streamAwait(resources.ipfs.getReadableStream(record.thumb2)),
          )
          promises.push(resources.ipfs.pin.add(record._id))
          promises.push(resources.ipfs.pin.add(record.thumb1))
          promises.push(resources.ipfs.pin.add(record.thumb2))
        }

        if (!externalContent?.payload?.value?.length) {
          promises.push(resources.model.content.put(record))
          promises.push(resources.model.feeds.external.add(record._id))
        }

        await Promise.all(promises)

        resources.dispatch(actions.feedContent.addExternal(record), user)

        res.send(`<html><body>${record.type} added.</body></html>`)
      } else {
        res.send(`<html><body>invalid record format.</body></html>`)
      }
    },
  )

  return router
}
