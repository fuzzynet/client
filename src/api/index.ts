import * as path from 'path'

import * as express from 'express'
import { Request } from 'express'
import * as cookieParser from 'cookie-parser'
import * as WebSocket from 'ws'

import { account } from 'api/account'
import { content } from 'api/content'
import { mobile } from 'api/mobile'
import { state } from 'api/state'
import { tags } from 'api/tags'
import { share } from 'api/share'

import * as Types from 'types/api'
import * as Crypto from 'lib/crypto'
import { Socket } from 'net'
import Future from 'lib/future'

export const expressApp = (
  ports: Types.Ports,
  appPath: string,
  resourcesFuture: Future<Types.Resources>,
  reduxLoadedFuture: Future<void>,
) => {
  const app = express()

  app.use('/static', express.static(path.resolve(appPath, '..', 'static')))

  app.get('/', (_req, res) => {
    res.sendFile(path.resolve(appPath, '../static/index.html'))
  })

  app.use('/api', state(resourcesFuture, reduxLoadedFuture))

  const server = app.listen(ports.http)

  return async (resources: Types.Resources) => {
    app.use(express.json())
    app.use(cookieParser())

    app.use('/api', account(resources))
    app.use('/api', content(resources))
    app.use('/api', mobile(resources))
    app.use('/api', tags(resources))
    app.use('/api', share(resources))

    app.get('*', async (_req, res) => {
      res.sendFile(path.resolve(resources.appPath, '../static/index.html'))
    })

    const wss = new WebSocket.Server({ noServer: true })

    server.on('upgrade', async (req: Request, socket: Socket, head: Buffer) => {
      try {
        const { user, account } = await Crypto.token(resources, req)
        console.info('auth ws for', user)
        wss.handleUpgrade(req, socket, head, (ws) => {
          console.log('new ws provided to user', user)
          wss.emit('connection', user)

          // init incoming ws handlers
          resources.wsUser.set(user, ws)
          const wsAccounts = resources.wsAccount.get(account) || []
          wsAccounts.push(user)
          resources.wsAccount.set(account, wsAccounts)
        })
      } catch (err) {
        console.error(err)
        socket.destroy()
      }
    })
  }
}
