import { Router, Request, Response } from 'express'
import * as _ from 'lodash'

import * as Types from 'types/api'

export const discover = (_resources: Types.Resources) => {
  const router = Router()

  router.get(Types.routes.get.discover(), (req: Request, res: Response) => {
    const data = req.params
    res.json(data)
  })

  return router
}
