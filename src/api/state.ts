import { Router, Request, Response } from 'express'
import * as _ from 'lodash'

import * as Types from 'types/api'
import Future from 'lib/future'

export const state = (
  resourcesFuture: Future<Types.Resources>,
  reduxLoadedFuture: Future<void>,
) => {
  const router = Router()

  router.get(Types.routes.get.state(), async (_req: Request, res: Response) => {
    const [resources] = await Promise.all([resourcesFuture, reduxLoadedFuture])
    const state = resources.redux.getState()
    res.json(state)
  })

  return router
}
