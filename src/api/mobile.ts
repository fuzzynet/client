import { Router, Request, Response } from 'express'
import * as internalIp from 'internal-ip'
import * as QRCode from 'qrcode'
// import * as ngrok from 'ngrok'
// import * as natpmp from 'lib/natpmp'

import * as Types from 'types/api'

// let ngrokurl = ''
// let ngrokdate = 0
// const ngrokttl = 8 * 60 * 60 * 1000 // measured in millis

// let natpmpurl = ''
// let natpmpdate = 0 // TODO: natpmp ttl renewal
// let natpmpttl = 2 * 60 * 60 // measured in seconds, reset if router gives us a different ttl

let lanerr = false
// let ngrokerr = false
// let natpmperr = false

export const mobile = (resources: Types.Resources) => {
  const router = Router()

  router.get(
    Types.routes.get.mobile(),
    async (_req: Request, res: Response) => {
      try {
        // lan
        const lanip = await internalIp.v4()
        const lanurl = `http://${lanip}:${resources.ports.http}/`

        // wan - ngrok
        // if (ngrokurl.length === 0 || ngrokdate < Date.now() - ngrokttl) {
        //   await ngrok.kill()

        //   try {
        //     ngrokurl = await ngrok.connect(resources.ports.http)
        //     ngrokdate = Date.now()
        //     ngrokerr = false
        //   } catch {
        //     ngrokurl = ''
        //     ngrokdate = 0
        //     ngrokerr = true
        //   }
        // }

        // wan - natpmp
        // try {
        //   const client = await natpmp.connect('10.0.1.1')

        //   const ipInfo = await client.externalIp()
        //   const wanip = ipInfo.ip.join()
        //   natpmpurl = `http://${wanip}:${resources.port}/`
        //   console.info('current external ip:', wanip)

        //   const pmInfo = await client.portMapping(
        //     'tcp',
        //     natpmpttl,
        //     resources.port,
        //   )
        //   natpmpttl = pmInfo.ttl || 2 * 60 * 60
        //   console.info('portmapping info:', pmInfo)
        //   natpmperr = false
        // } catch (err) {
        //   console.error('portmapping error', err)
        //   natpmperr = true
        // }
        // natpmperr = true // TODO: fix or find an alternative to nat-pmp.

        const [lanqr] = await Promise.all([
          QRCode.toDataURL(lanurl || 'lan error'),
          // QRCode.toDataURL(ngrokurl || 'ngrok error'),
          // QRCode.toDataURL(natpmpurl || 'natpmp error'),
        ])

        const data: Types.GetMobileResponse = {
          qr: { lan: lanqr },
          url: { lan: lanurl },
          err: { lan: lanerr },
        }

        res.json(data)
      } catch (err) {
        console.error(err)
        res.sendStatus(500)
      }
    },
  )

  return router
}
