// create a lookup table that maps byte-to-byte hamming distance values to 32-bit hex strings
interface ByteHexLookup {
  [hex32: number]: string
}

const byteHexLookup: ByteHexLookup = {}
const bytes = 2 ** 8

for (let i = 0; i < bytes; i++) {
  byteHexLookup[i] = i.toString(16).padStart(2, '0')
}

interface DistanceLookup {
  [hex32: string]: number
}

const distanceLookup: DistanceLookup = {}

for (let a = 0; a < bytes; a++) {
  for (let b = 0; b < bytes; b++) {
    distanceLookup[byteHexLookup[a] + byteHexLookup[b]] = Math.abs(a - b)
  }
}

// calculate hamming distance of two hex strings of equal length
export const distance = (a: string, b: string) => {
  // given: 1. both a and b are the same length
  // and, 2. both a and b are hex-encoded byte vectors
  // and, 3. these hex values are lowercase

  let d = 0
  for (let i = 0; i < a.length; i += 2) {
    d += distanceLookup[a.substr(i, 2) + b.substr(i, 2)]
  }
  return d
}
