// based on: https://github.com/skedastik/ghash/blob/595e3c93c7c31e18d7136d4ce2f5886317df2719/index.js

const RESOLUTION = 32
const OUTPUT_BUF_SIZE = (RESOLUTION * RESOLUTION) / 8
const MIN_FUZZINESS = 0
// const MIN_RESOLUTION = 2
// const MAX_FUZZINESS = 255

export const ghash = (inputBuf: Buffer, fuzziness = MIN_FUZZINESS): Buffer => {
  const outputBuf = Buffer.alloc(OUTPUT_BUF_SIZE, 0)
  const iters = inputBuf.length - 1

  let octet = 0
  let bit = 0

  // calculate hash from luminance gradient
  for (let i = 0; i < iters; i++) {
    if (inputBuf[i + 1] - inputBuf[i] > fuzziness) {
      outputBuf[octet] |= 1 << bit
    }
    if (++bit === 8) {
      octet++
      bit = 0
    }
  }

  // wrap to first pixel
  if (inputBuf[0] - inputBuf[iters] > fuzziness) {
    outputBuf[octet] |= 1 << bit
  }

  return outputBuf
}
