import * as sharp from 'sharp'

const RESOLUTION = 32

import { ghash } from './ghash'
import { phash } from './phash'

export interface PerceptualHashes {
  ghash: string // 64-bit monochrome hex hash string
}

export const hashAll = async (imageData: Buffer) => {
  const grayscale = await sharp(imageData)
    .resize(RESOLUTION, RESOLUTION, { fit: 'fill' })
    .flatten()
    .grayscale()
    .raw()
    .toBuffer()

  return {
    ghash: ghash(grayscale),
    phash: phash(grayscale),
  }
}

export * from './distance'
