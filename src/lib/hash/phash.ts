// based on: https://github.com/btd/sharp-phash/blob/09ba75b650ba91114b065b4d27dc4fc81e373b27/index.js

const RESOLUTION = 32

const initSQRT = (N: number): number[] => {
  let c = new Array(N)
  for (let i = 1; i < N; i++) {
    c[i] = 1
  }
  c[0] = 1 / Math.sqrt(2.0)
  return c
}

const SQRT = initSQRT(RESOLUTION)

const initCOS = (N: number): number[][] => {
  let cosines = new Array(N)
  for (let k = 0; k < N; k++) {
    cosines[k] = new Array(N)
    for (let n = 0; n < N; n++) {
      cosines[k][n] = Math.cos(((2 * k + 1) / (2.0 * N)) * n * Math.PI)
    }
  }
  return cosines
}

const COS = initCOS(RESOLUTION)

const applyDCT = (f: number[][], size: number): number[][] => {
  var N = size

  var F: number[][] = new Array(N)
  for (var u = 0; u < N; u++) {
    F[u] = new Array(N)
    for (var v = 0; v < N; v++) {
      var sum = 0
      for (var i = 0; i < N; i++) {
        for (var j = 0; j < N; j++) {
          sum += COS[i][u] * COS[j][v] * f[i][j]
        }
      }
      sum *= (SQRT[u] * SQRT[v]) / 4
      F[u][v] = sum
    }
  }
  return F
}

const LOW_SIZE = 8

export const phash = (data: Buffer): Buffer => {
  // copy signal
  const s: number[][] = new Array(RESOLUTION)
  for (let x = 0; x < RESOLUTION; x++) {
    s[x] = new Array(RESOLUTION)
    for (let y = 0; y < RESOLUTION; y++) {
      s[x][y] = data[RESOLUTION * y + x]
    }
  }

  // apply 2D DCT II
  const dct: number[][] = applyDCT(s, RESOLUTION)

  // get AVG on high frequencies
  let totalSum = 0
  for (let x = 0; x < LOW_SIZE; x++) {
    for (let y = 0; y < LOW_SIZE; y++) {
      totalSum += dct[x + 1][y + 1]
    }
  }

  // compute hash
  const OUTPUT_BUF_SIZE = LOW_SIZE * LOW_SIZE
  const avg = totalSum / OUTPUT_BUF_SIZE
  const fingerprint: string[] = []

  for (let x = 0; x < LOW_SIZE; x++) {
    for (let y = 0; y < LOW_SIZE; y++) {
      fingerprint.push(dct[x + 1][y + 1] > avg ? '1' : '0')
    }
  }

  // convert to buffer
  const BYTE_LENGTH = 8
  const OUTPUT_BYTE_LENGTH = OUTPUT_BUF_SIZE / BYTE_LENGTH

  const outBuf = Buffer.alloc(LOW_SIZE, 0)

  for (let i = 0; i < OUTPUT_BYTE_LENGTH; i++) {
    outBuf[i] = parseInt(
      fingerprint.slice(i * BYTE_LENGTH, (i + 1) * BYTE_LENGTH).join(''),
      2,
    )
  }

  return outBuf
}
