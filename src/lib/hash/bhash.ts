// based on: https://github.com/LinusU/blockhash-core/blob/7cce02968abb817787cf12753d33d7ce9f787c77/index.js

const median = (data: number[]): number => {
  const mdarr = data.slice(0)
  mdarr.sort((a: number, b) => a - b)

  if (mdarr.length % 2 === 0) {
    return (mdarr[mdarr.length / 2 - 1] + mdarr[mdarr.length / 2]) / 2.0
  }

  return mdarr[Math.floor(mdarr.length / 2)]
}

const translateBlocksToBits = (
  blocks: number[],
  pixelsPerBlock: number,
): void => {
  const halfBlockValue = (pixelsPerBlock * 256 * 3) / 2
  const bandsize = blocks.length / 4

  // Compare medians across four horizontal bands
  for (let i = 0; i < 4; i++) {
    const m = median(blocks.slice(i * bandsize, (i + 1) * bandsize))
    for (let j = i * bandsize; j < (i + 1) * bandsize; j++) {
      const v = blocks[j]

      // Output a 1 if the block is brighter than the median.
      // With images dominated by black or white, the median may
      // end up being 0 or the max value, and thus having a lot
      // of blocks of value equal to the median.  To avoid
      // generating hashes of all zeros or ones, in that case output
      // 0 if the median is in the lower value space, 1 otherwise
      blocks[j] = Number(v > m || (Math.abs(v - m) < 1 && m > halfBlockValue))
    }
  }
}

const bitsToHexhash = (bitsArray: number[]): string => {
  const hex = []

  for (let i = 0; i < bitsArray.length; i += 4) {
    const nibble = bitsArray.slice(i, i + 4)
    hex.push(parseInt(nibble.join(''), 2).toString(16))
  }

  return hex.join('')
}

const bmvbhashEven = (
  data: Buffer,
  width: number,
  height: number,
  bits: number,
): string => {
  const blocksizeX = Math.floor(width / bits)
  const blocksizeY = Math.floor(height / bits)

  const result: number[] = []

  for (let y = 0; y < bits; y++) {
    for (let x = 0; x < bits; x++) {
      let total = 0

      for (let iy = 0; iy < blocksizeY; iy++) {
        for (let ix = 0; ix < blocksizeX; ix++) {
          const cx = x * blocksizeX + ix
          const cy = y * blocksizeY + iy
          const ii = (cy * width + cx) * 4

          const alpha = data[ii + 3]
          total += alpha === 0 ? 765 : data[ii] + data[ii + 1] + data[ii + 2]
        }
      }

      result.push(total)
    }
  }

  translateBlocksToBits(result, blocksizeX * blocksizeY)

  return bitsToHexhash(result)
}

export const bmvbhash = (
  data: Buffer,
  width: number,
  height: number,
  bits = 16,
): string => {
  const result: number[] = []

  let blockWidth: number, blockHeight: number
  let weightTop, weightBottom, weightLeft, weightRight
  let blockTop, blockBottom, blockLeft, blockRight
  let yMod, yFrac, yInt
  let xMod, xFrac, xInt
  const blocks: number[][] = []

  const evenX = width % bits === 0
  const evenY = height % bits === 0

  if (evenX && evenY) {
    return bmvbhashEven(data, width, height, bits)
  }

  // initialize blocks array with 0s
  for (let i = 0; i < bits; i++) {
    blocks.push([])
    for (let j = 0; j < bits; j++) {
      blocks[i].push(0)
    }
  }

  blockWidth = width / bits
  blockHeight = height / bits

  for (let y = 0; y < height; y++) {
    if (evenY) {
      // don't bother dividing y, if the size evenly divides by bits
      blockTop = blockBottom = Math.floor(y / blockHeight)
      weightTop = 1
      weightBottom = 0
    } else {
      yMod = (y + 1) % blockHeight
      yFrac = yMod - Math.floor(yMod)
      yInt = yMod - yFrac

      weightTop = 1 - yFrac
      weightBottom = yFrac

      // yInt will be 0 on bottom/right borders and on block boundaries
      if (yInt > 0 || y + 1 === height) {
        blockTop = blockBottom = Math.floor(y / blockHeight)
      } else {
        blockTop = Math.floor(y / blockHeight)
        blockBottom = Math.ceil(y / blockHeight)
      }
    }

    for (let x = 0; x < width; x++) {
      const ii = (y * width + x) * 4

      const alpha = data[ii + 3]
      const avgvalue =
        alpha === 0 ? 765 : data[ii] + data[ii + 1] + data[ii + 2]

      if (evenX) {
        blockLeft = blockRight = Math.floor(x / blockWidth)
        weightLeft = 1
        weightRight = 0
      } else {
        xMod = (x + 1) % blockWidth
        xFrac = xMod - Math.floor(xMod)
        xInt = xMod - xFrac

        weightLeft = 1 - xFrac
        weightRight = xFrac

        // xInt will be 0 on bottom/right borders and on block boundaries
        if (xInt > 0 || x + 1 === width) {
          blockLeft = blockRight = Math.floor(x / blockWidth)
        } else {
          blockLeft = Math.floor(x / blockWidth)
          blockRight = Math.ceil(x / blockWidth)
        }
      }

      // add weighted pixel value to relevant blocks
      blocks[blockTop][blockLeft] += avgvalue * weightTop * weightLeft
      blocks[blockTop][blockRight] += avgvalue * weightTop * weightRight
      blocks[blockBottom][blockLeft] += avgvalue * weightBottom * weightLeft
      blocks[blockBottom][blockRight] += avgvalue * weightBottom * weightRight
    }
  }

  for (let i = 0; i < bits; i++) {
    for (let j = 0; j < bits; j++) {
      result.push(blocks[i][j])
    }
  }

  translateBlocksToBits(result, blockWidth * blockHeight)

  return bitsToHexhash(result)
}
