import * as util from 'util'
import * as crypto from 'crypto'

import * as express from 'express'
import { JWK } from 'node-jose'
import * as jwt from 'jsonwebtoken'

import * as Types from 'types/api'

const randomBytes = util.promisify(crypto.randomBytes)

export const getCookie = (name: string, cookies = ''): string => {
  const nameLenPlus = name.length + 1
  return cookies
    .split(';')
    .map((c) => c.trim())
    .filter((cookie) => {
      return cookie.substring(0, nameLenPlus) === `${name}=`
    })
    .map((cookie) => {
      return decodeURIComponent(cookie.substring(nameLenPlus))
    })[0]
}

export const token = async (
  resources: Types.Resources,
  req: express.Request,
  res?: express.Response,
): Promise<Types.Token> => {
  const token = getCookie('token', req.headers.cookie) || ''
  let data: Types.Token

  const { jwk, account } = resources.crypto

  if (token.length) {
    try {
      data = jwt.verify(token, jwk as any, {
        algorithms: ['ES512'],
      }) as Types.Token
      console.info('token verified', data)
    } catch (err) {
      console.info(err)

      data = { user: await randomBytes32(), account }
      const newToken = jwt.sign(data, jwk as any, {
        algorithm: 'ES512',
      })

      if (res) {
        res.cookie('token', newToken)
      }
    }
  } else {
    data = { user: await randomBytes32(), account }
    console.info('token issued', data)
    const newToken = jwt.sign(data, jwk as any, {
      algorithm: 'ES512',
    })

    if (res) {
      res.cookie('token', newToken)
    }
  }

  return data
}

export const randomBytes32 = async () => {
  const buf = await randomBytes(16)
  return buf.toString('hex')
}

export const init = async (): Promise<Types.Crypto> => {
  let jwk: JWK.Key

  try {
    jwk = await JWK.createKey('EC', 'P-521', { alg: 'ES512' })
    console.info('session jwk', jwk)
  } catch (err) {
    console.error(err)
    throw new Error(err)
  }

  return {
    jwk: crypto.createPrivateKey(jwk.toPEM(true)),
    account: await randomBytes32(),
  }
}
