// example usage: logger.info(thing, 'things about thing')

type Level = keyof Console
type Logger = { [L in Level]: <T>(orig: T, ...message: any[]) => T }

const levels: Level[] = Object.keys(console) as Level[]

export const logger = levels.reduce((acc: Logger, level: Level): Logger => {
  acc[level] = <T>(orig: T, ...message: any[]) => {
    console[level](...message, orig)
    return orig
  }
  return acc
}, {} as Logger)
