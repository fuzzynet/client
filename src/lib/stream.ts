import { Readable } from 'stream'

export const streamAwait = (stream: Readable): Promise<number> =>
  new Promise((resolve, reject) => {
    let length = 0
    let resolved = false
    stream.on('data', (chunk) => {
      length += chunk.length
    })
    stream.on('end', () => {
      resolved = true
      console.info(length, 'bytes read')
      if (!resolved) {
        resolve(length)
      }
    })
    stream.on('error', (err: Error) => {
      console.error(err)
      resolved = true
      if (!resolved) {
        reject(err)
      }
    })
  })
