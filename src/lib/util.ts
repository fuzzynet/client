export const range = (length: number) => Array.from({ length }, (_x, i) => i)
