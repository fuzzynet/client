// adapted from: https://stackoverflow.com/a/40356701

export default class Future<T> implements PromiseLike<T> {
  // @ts-ignore
  private promise: Promise<T>
  // @ts-ignore
  private resolveFunction: (value?: T | PromiseLike<T>) => void
  // @ts-ignore
  private rejectFunction: (reason?: any) => void

  constructor(promise?: Promise<T>) {
    if (!(this instanceof Future)) {
      return new Future(promise)
    }

    this.promise = promise || new Promise(this.promiseExecutor.bind(this))
  }

  public isPending = true
  public isRejected = false
  public isResolved = false

  public asPromise(): Promise<T> {
    return this.promise
  }

  public then<TResult>(
    onfulfilled?: (value: T) => TResult | PromiseLike<TResult>,
    onrejected?: (reason: any) => TResult | PromiseLike<TResult>,
  ): Future<TResult>
  public then<TResult>(
    onfulfilled?: (value: T) => TResult | PromiseLike<TResult>,
    onrejected?: (reason: any) => void,
  ): Future<TResult>
  public then<TResult>(
    onfulfilled?: (value: T) => TResult | PromiseLike<TResult>,
    onrejected?: (reason: any) => any,
  ): Future<TResult> {
    return new Future(this.promise.then(onfulfilled, onrejected))
  }

  public catch(onrejected?: (reason: any) => T | PromiseLike<T>): Future<T>
  public catch(onrejected?: (reason: any) => void): Future<T>
  public catch(onrejected?: (reason: any) => any): Future<T> {
    return new Future(this.promise.catch(onrejected))
  }

  public resolve(value?: T | PromiseLike<T>) {
    this.isPending = false
    this.isResolved = true
    this.resolveFunction(value)
  }

  public reject(reason?: any) {
    this.isPending = false
    this.isRejected = true
    this.rejectFunction(reason)
  }

  private promiseExecutor(
    resolve: (value?: T | PromiseLike<T>) => void,
    reject: (reason?: any) => void,
  ) {
    this.resolveFunction = resolve
    this.rejectFunction = reject
  }
}
