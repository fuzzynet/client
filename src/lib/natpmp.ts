/*
  node.js implementation of the nat port mapping protocol (a.k.a nat-pmp).

  references:
    - http://miniupnp.free.fr/nat-pmp.html
    - http://wikipedia.org/wiki/NAT_Port_Mapping_Protocol
    - http://tools.ietf.org/html/draft-cheshire-nat-pmp-03
*/

import * as util from 'util'
import * as assert from 'assert'
import * as dgram from 'dgram'

import Future from './future'

// natpmp spec version
const VERSION = 0

// the ports defined in draft-cheshire-nat-pmp-03 to send nat-pmp requests to.
const CLIENT_PORT = 5350
const SERVER_PORT = 5351

// the opcodes for client requests.
const OP_EXTERNAL_IP = 0
const OP_MAP_UDP = 1
const OP_MAP_TCP = 2

// map of result codes the gateway sends back when mapping a port.
interface ResultCodes {
  [key: number]: string
}

export const RESULT_CODES: ResultCodes = {
  0: 'Success',
  1: 'Unsupported Version',
  2: 'Not Authorized/Refused (gateway may have NAT-PMP disabled)',
  3: 'Network Failure (gateway may have not obtained a DHCP lease)',
  4: 'Out of Resources (no ports left)',
  5: 'Unsupported opcode',
}

// creates a client instance. familiar api to `net.connect()`.
export const connect = async (gateway: string) => {
  const client = new Client(gateway)
  await client.connect()
  return client
}

// typings

interface RES {
  op: number
  msg: Buffer
  resultCode: number
  resultMessage: string
  vers: number
  epoch: number
  remoteAddressInfo: dgram.RemoteInfo
}

interface IPRES extends RES {
  ip: number[]
}

interface PMRES extends RES {
  protocol: string
  external: number
  internal: number
  ttl: number
}

export class Client {
  gateway: string

  socket?: dgram.Socket
  socketBind: (port: number) => Promise<void>
  socketSend: (
    msg: string | Uint8Array,
    offset: number,
    length: number,
    port: number,
    address: string,
  ) => Promise<number>

  listening = false

  _ipFuture = new Future<IPRES>()
  _pmFuture = new Future<PMRES>()
  _ipOp?: number
  _pmOp?: number
  _connecting = true

  constructor(gateway: string) {
    console.debug('creating new Client instance for gateway', gateway)

    this.gateway = gateway
    this.socket = dgram.createSocket('udp4')
    this.socketBind = util.promisify(this.socket.bind)
    this.socketSend = util.promisify(this.socket.send)

    this.socket.on('listening', this.onListening)
    this.socket.on('message', this.onMessage)
    this.socket.on('close', this.onClose)
    this.socket.on('error', this.onError(undefined))
  }

  // binds to the natpmp client port.
  async connect() {
    console.debug('Client#connect()')

    if (this._connecting) {
      return
    }

    this._connecting = true

    await this.socketBind(CLIENT_PORT)

    this.onListening()
  }

  // sends a udp request to be send to the gateway device.
  async send(buf: Buffer) {
    return await this.socketSend(buf, 0, buf.length, SERVER_PORT, this.gateway)
  }

  // sends a request to the server for the current external ip address.
  async externalIp(): Promise<IPRES> {
    console.debug('Client#externalIp()')

    let pos = 0
    const buf = Buffer.alloc(2)
    buf.writeUInt8(VERSION, pos)
    pos++
    buf.writeUInt8(OP_EXTERNAL_IP, pos)

    let bytes = 0
    try {
      bytes = await this.send(buf)
      this._ipFuture = new Future<IPRES>()
    } catch (err) {
      if (bytes !== buf.length) {
        this.onError(OP_EXTERNAL_IP)(
          new Error('entire request buffer not sent. This should not happen!'),
        )
      } else {
        this.onError(OP_EXTERNAL_IP)(err)
      }
    }

    return await this._ipFuture.asPromise()
  }

  // sets up a new port mapping.
  async portMapping(
    protocol: string,
    ttl: number,
    internal: number,
    external?: number,
  ) {
    const op = protocol === 'udp' ? OP_MAP_UDP : OP_MAP_TCP

    let pos = 0
    const buf = Buffer.alloc(12)
    buf.writeUInt8(0, pos)
    pos++ // vers = 0
    buf.writeUInt8(op, pos)
    pos++ // op = x
    buf.writeUInt16BE(0, pos)
    pos += 2 // reserved (must be zero)
    buf.writeUInt16BE(internal, pos)
    pos += 2 // internal port
    buf.writeUInt16BE(external || internal, pos)
    pos += 2 // requested external port
    buf.writeUInt32BE(ttl, pos)
    pos += 4 // requested port mapping lifetime in seconds

    let bytes = 0
    try {
      bytes = await this.send(buf)
      this._pmFuture = new Future<PMRES>()
    } catch (err) {
      if (bytes !== buf.length) {
        this.onError(op)(
          new Error('Entire request buffer not sent. This should not happen!'),
        )
      } else {
        this.onError(op)(err)
      }
    }

    return await this._pmFuture.asPromise()
  }

  // to unmap a port you simply set the ttl to 0.
  public async portUnmapping(
    protocol: string,
    internal: number,
    external?: number,
  ) {
    return await this.portMapping(protocol, 0, internal, external)
  }

  // closes the underlying socket.
  close() {
    console.debug('Client#close()')
    if (this.socket) {
      this.socket.close()
    }
  }

  // called for the underlying socket's "listening" event.
  onListening() {
    console.debug('Client#onListening()')
    this.listening = true
    this._connecting = false
  }

  // called for the underlying socket's "message" event.
  onMessage(msg: Buffer, remoteAddressInfo: dgram.RemoteInfo) {
    console.debug('Client#onMessage()', msg, remoteAddressInfo)

    if ((this._ipFuture.isPending || this._pmFuture.isPending) === false) {
      console.info('Client#onMessage(): ignoring unexpected message')
      return
    }

    let pos = 0
    const vers = msg.readUInt8(pos)
    pos++
    const op = msg.readUInt8(pos)
    pos++

    try {
      if (vers !== VERSION) {
        throw new Error(`"vers" must be 0. Got: ${vers}`)
      }

      // common fields
      const resultCode = msg.readUInt16BE(pos)
      pos += 2
      const resultMessage = RESULT_CODES[resultCode]
      const epoch = msg.readUInt32BE(pos)
      pos += 4

      const response: RES = {
        op,
        msg,
        resultCode,
        resultMessage,
        vers,
        epoch,
        remoteAddressInfo,
      }

      if (resultCode === 0) {
        // success response
        switch (op) {
          case OP_EXTERNAL_IP:
            let ip: number[] = []

            ip.push(msg.readUInt8(pos))
            pos++
            ip.push(msg.readUInt8(pos))
            pos++
            ip.push(msg.readUInt8(pos))
            pos++
            ip.push(msg.readUInt8(pos))
            pos++

            this._ipFuture.resolve({ ...response, ip })
            break
          case OP_MAP_UDP:
          case OP_MAP_TCP:
            const internal = msg.readUInt16BE(pos)
            pos += 2
            const external = msg.readUInt16BE(pos)
            pos += 2
            const ttl = msg.readUInt32BE(pos)
            pos += 4
            const protocol = op === OP_MAP_UDP ? 'udp' : 'tcp'

            this._pmFuture.resolve({
              ...response,
              internal,
              external,
              ttl,
              protocol,
            })
            break
          default: {
            throw new Error(`unknown OP code: ${op}`)
          }
        }

        assert.equal(
          msg.length,
          pos,
          'was not able to process entire message received',
        )
      } else {
        // error response
        throw new Error(`${resultMessage}: ${resultCode}`)
      }
    } catch (err) {
      this.onError(op)(new Error(`"vers" must be 0. Got: ${vers}`))
    }
  }

  // called for the underlying socket's "close" event.
  onClose() {
    console.debug('Client#onClose()')
    this.listening = false
    this.socket = undefined
  }

  // called for the underlying socket's "error" event.
  onError(op?: number) {
    return (err: Error) => {
      console.debug('Client#onerror()', err)
      if (op === OP_EXTERNAL_IP && this._ipFuture.isPending) {
        this._ipFuture.reject(err)
        console.error('error getting external ip', err)
      } else if (
        (op === OP_MAP_UDP || op === OP_MAP_TCP) &&
        this._pmFuture.isPending
      ) {
        this._pmFuture.reject(err)
        console.error('port mapping error', err)
      } else {
        console.error('unknown error', err)
      }
    }
  }
}
