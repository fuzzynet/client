import * as path from 'path'
import * as webpack from 'webpack'

const config: webpack.Configuration = {
  target: 'web',
  mode: 'development',
  entry: './dist/ui/index.js',
  module: {
    rules: [
      { test: /\.url$/, use: 'url-loader' },
      { test: /\.file$/, use: 'file-loader' },
    ],
  },
  output: {
    path: path.resolve(__dirname, 'static', 'dist'),
    filename: 'app.js',
  },
}

export default config
